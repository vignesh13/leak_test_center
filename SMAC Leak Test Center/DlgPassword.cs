﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SMAC_Leak_Test_Center
{
    //
    // ShowDialog() can have the following return values:
    //
    // DialogResult.Cancel - No valid pasword enterted and Cancel was pressed (or ESC)
    // DialogResult.OK     - Password was valid and the checkbox "Keep me logged in" was not checked
    // DialogResult.Yes    - Password was valid and the checkbox "Keep me logged in" was checked
    //
    public partial class DlgPassword : Form
    {
        private string pass = "";
        public DlgPassword(string password)
        {
            pass = password;
            InitializeComponent();
            //            label2.Text += pass;
            label2.Text = "";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text != pass)
            {
                MessageBox.Show("Password not accepted", "Invalid password", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (chkKeepMeLoggedIn.Checked)
                {
                    DialogResult = DialogResult.Yes;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
            txtPassword.Text = "";
            txtPassword.Focus();
        }
    }
}
