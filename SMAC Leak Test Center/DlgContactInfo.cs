﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace SMAC_Leak_Test_Center
{
    public partial class DlgContactInfo : Form
    {
        public DlgContactInfo()
        {
            InitializeComponent();
            
        }


        private void linkSmacInternational_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.smac-mca.com");
            DialogResult = DialogResult.OK;
        }

    }
}
