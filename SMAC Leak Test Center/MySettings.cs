﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace SMAC_Leak_Test_Center
{
    class MySettings
    {
        private const string SECTION_SETTINGS = "Settings";
        private const string KEY_ACTUATOR = "Actuator";
        private const string KEY_ORIENTATION = "Orientation";
        private const string KEY_TUNING_LINEAR_P = "Tuning linear P";
        private const string KEY_TUNING_LINEAR_I = "Tuning linear I";
        private const string KEY_TUNING_LINEAR_D = "Tuning linear D";
        private const string KEY_LEAK_TEST_TYPE = "Leak test Type";
        private const string KEY_SOFTLAND_SPEED = "Softland Speed";
        private const string KEY_POSITION_CLOSE_TO_PART = "Position close to part";
        private const string KEY_CHECK_LANDED_HEIGHT = "Check landed height";
        private const string KEY_LANDED_HEIGHT_MIN = "Landed height min";
        private const string KEY_LANDED_HEIGHT_MAX = "Landed height max";
        private const string KEY_SPEED = "Speed";
        private const string KEY_SENSITIVITY_VM = "VM_Sensitivity";
        private const string KEY_SENSITIVITY_FM = "FM_Sensitivity";
        private const string KEY_TEACHED_SQ_LINEAR = "Teached SQ Linear";
       
        private const string KEY_TEACHED_SQ_LINEAR_HOME = "Teached SQ Linear Home";
        private const string KEY_TEACHED_LANDED_DIST = "Teached landed distance";
        private const string KEY_PUSH_FORCE = "Push Force";
        private const string KEY_SOFT_LAND_FORCE = "Softland Force";
        private const string KEY_DWELL_TIME = "Dwell Time";
        private const string KEY_PARK_POSITION = "Park Position";

        private const string KEY_SOFTLAND_POSITION = "Softland Position";
        private const string KEY_MAX_DISP_LIM = "Max Displacement Limit";
        //private const string KEY_RESOLUTION = "Resolution";
        


        public MySettings()       // The constructor
        {
        }

        public void Read(string path)
        {
            IniFile inifile = new IniFile(path);
            try
            {
                Globals.mainForm.cmbActuator.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_ACTUATOR);
                Globals.mainForm.ibdPushForce.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_PUSH_FORCE);
                Globals.mainForm.ibdSoftLandForce.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SOFT_LAND_FORCE);  
                Globals.mainForm.ibdTuningLinearP.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_P);
                Globals.mainForm.ibdTuningLinearI.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_I);
                Globals.mainForm.ibdTuningLinearD.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_D);
                Globals.mainForm.cmbLeakTestType.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_LEAK_TEST_TYPE);
                Globals.mainForm.ibdPositonCloseToContainer.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_POSITION_CLOSE_TO_PART);
                Globals.mainForm.chkCheckLandedDistance.Checked = inifile.ReadBoolValue(SECTION_SETTINGS, KEY_CHECK_LANDED_HEIGHT);
                Globals.mainForm.ibdLandedHeightMin.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MIN);
                Globals.mainForm.ibdSoftlandPosition.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SOFTLAND_POSITION);
                Globals.mainForm.ibdLandedHeightMax.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MAX);

                Globals.mainForm.ibdSoftlandSpeed.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SOFTLAND_SPEED);
                Globals.mainForm.ibdSpeed.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SPEED);
                Globals.mainForm.ibdSoftlandSensitivity.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SENSITIVITY_VM);
                Globals.mainForm.ibdSoftlandSensitivity2.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SENSITIVITY_FM);
                Globals.mainForm.txtTeachedSqLinear.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR);
                Globals.mainForm.txtTeachedSqLinearHome.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR_HOME);
                
                Globals.mainForm.txtTeachedLandedDis.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_LANDED_DIST);
                Globals.mainForm.ibdDwellTime.Doublevalue = inifile.ReadIntValue(SECTION_SETTINGS, KEY_DWELL_TIME);
                Globals.mainForm.ibdParkPosition.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_PARK_POSITION);

                Globals.mainForm.ibdMaxDispLim.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_MAX_DISP_LIM);
              //  Globals.mainForm.ibdSetResolution.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_RESOLUTION);  



            }
            catch
            {
                MessageBox.Show("Error reading settings file " + path , "File I/O error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Write(string path)
        {
            IniFile inifile = new IniFile(path);
            try
            {
                inifile.WriteValue(SECTION_SETTINGS, KEY_ACTUATOR, Globals.mainForm.cmbActuator.SelectedItem.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_P, Globals.mainForm.ibdTuningLinearP.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_I, Globals.mainForm.ibdTuningLinearI.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_D, Globals.mainForm.ibdTuningLinearD.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_LEAK_TEST_TYPE, Globals.mainForm.cmbLeakTestType.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_POSITION_CLOSE_TO_PART, Globals.mainForm.ibdPositonCloseToContainer.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_CHECK_LANDED_HEIGHT, Globals.mainForm.chkCheckLandedDistance.Checked.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MIN, Globals.mainForm.ibdLandedHeightMin.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MAX, Globals.mainForm.ibdLandedHeightMax.Stringvalue);


                inifile.WriteValue(SECTION_SETTINGS, KEY_PUSH_FORCE, Globals.mainForm.ibdPushForce.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SOFT_LAND_FORCE, Globals.mainForm.ibdSoftLandForce.Stringvalue);
              
                inifile.WriteValue(SECTION_SETTINGS, KEY_SOFTLAND_SPEED, Globals.mainForm.ibdSoftlandSpeed.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SPEED, Globals.mainForm.ibdSpeed.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SENSITIVITY_VM, Globals.mainForm.ibdSoftlandSensitivity.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SENSITIVITY_FM, Globals.mainForm.ibdSoftlandSensitivity2.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR, Globals.mainForm.txtTeachedSqLinear.Text);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR_HOME, Globals.mainForm.txtTeachedSqLinearHome.Text);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_LANDED_DIST, Globals.mainForm.txtTeachedLandedDis.Text);
                inifile.WriteValue(SECTION_SETTINGS, KEY_DWELL_TIME, Globals.mainForm.ibdDwellTime.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_PARK_POSITION, Globals.mainForm.ibdParkPosition.Stringvalue);

                inifile.WriteValue(SECTION_SETTINGS, KEY_SOFTLAND_POSITION, Globals.mainForm.ibdSoftlandPosition.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_MAX_DISP_LIM, Globals.mainForm.ibdMaxDispLim.Stringvalue);
            }
            catch
            {
                MessageBox.Show("Error writing settings file " + path , "File I/O error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
