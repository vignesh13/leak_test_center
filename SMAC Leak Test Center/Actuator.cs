﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace SMAC_Leak_Test_Center
{

    public class Actuator
    {
        private string strName = ""; // holds ini file name for checking
        private double linearResolution; // holds resolution
        private double linearStroke; // holds stroke length

      
                                        //  private int maxPushForce;  // holds maxPushForce
        private double linearMaxDispLim;
        private int  linearDwellTime;

        private string fullPath = ""; // holds path to file
        public Variables variables = null;  // holds all variables

        //
        // Construct an actuator and read the settings from an ini file
        // Input ini file containing stroke, rotary, and PID settings
        //
        public void Load(string filename)
        {
            // MessageBox.Show(Environment.CurrentDirectory);
            // replaces exclamation mark with colon mark in file name
            filename = filename.Replace(':', '!');
            variables = null;
            if (Globals.mainForm.cmbController.Text == "LAC-1")
            {
                // searches each file in the actuator config directory until chosen one is found
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators\\LAC1", "*.ini*", SearchOption.AllDirectories))
                {
                    if (Path.GetFileNameWithoutExtension(s) == filename)
                    {
                        strName = filename.Replace('!', ':');
                        fullPath = s;

                        variables = new Variables(Constants.Defaults()); // loads default stroke, rotary, and PID settings
                        IniFile inifile = new IniFile(fullPath);

                        // get basic values from file
                        linearResolution = inifile.ReadDoubleValue("Linear", "Resolution");
                        linearStroke = inifile.ReadDoubleValue("Linear", "Stroke");
                        linearMaxDispLim = inifile.ReadDoubleValue("Linear", "MaxDispLim");
                        linearDwellTime = inifile.ReadIntValue("Linear", "DwellTime");  
                        
                      //  maxPushForce = inifile.ReadIntValue("Linear", "maxPushForce");
                        ReadOtherVariables("Constants");
                        break;
                    }
                }
            }
           
        }


        public void ReadOtherVariables(string Section)
        {
            IniFile inifile = new IniFile(fullPath);
            foreach (string s in inifile.ReadSectionKeynames(Section))
            {
                variables.SetValue(s, inifile.ReadIntValue(Section, s));
            }
        }

        public string[] Info()
        {
            List<string> info = new List<string>();

            info.Add("Actuator: " +strName);
            info.Add("");
            info.Add("Linear:");
            info.Add("Encoder resolution: " + linearResolution.ToString().Replace(',', ',') + " um");
            info.Add("Stroke : " + linearStroke.ToString().Replace(',', '.') + " mm");
            info.Add("Max Disp Limit :" + linearMaxDispLim.ToString() + "mm");
            info.Add("Dwell Time :" + linearDwellTime.ToString().Replace(',', ',') + "ms");
           
          //  info.Add("Max Push Force: " + maxPushForce.ToString());
            return info.ToArray();
        }

        public string Name()
        {
            return strName;
        }

        public double LinearResoloution()
        {
            return linearResolution ;
        }

        public double LinearStroke()
        {
            return linearStroke;
        }

        public double LinearMaxDispLim()
        {
          return linearMaxDispLim; 
        }

        
       public double LinearDwellTime()
       {
            return linearDwellTime;
       }
        public Variables GetVariables()
        {
            return variables;
        }
    }
}
