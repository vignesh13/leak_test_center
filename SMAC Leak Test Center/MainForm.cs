﻿//#define SHOW_LISTBOX1
//#define SHOW_TEXTBOX2

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Diagnostics;


namespace SMAC_Leak_Test_Center
{

    public partial class MainForm : Form
    {

        private const string STR_MM = "mm";
        private const string STR_TPI = "TPI";
        public string ActuatorFoldername = "";
        private string saveFile = "";

        private const string LAC1 = "LAC-1";
       // private const string LAC25 = "LAC-25";
        private bool passwordRequired = true;  // TODO REMOVE IN FINAL VERSION

        public const string programName = "SMAC Leak Test Center 0.17";
        private List<string> header = new List<string>();

        private delegate void SetTextDeleg(string text);
        public Serial port = new Serial();

        private string fileName = "";
       

        public bool NoActuatorfilesFound = false;
        private bool ignoreResponse = false;        // Ignores responses from the controller that start with !
        private bool checkFirst = false;            // True if the next character must be checked for !
        private string lastCommand = "";
        
        public InputBoxDouble ibdPositonCloseToContainer = null;
        public InputBoxDouble ibdLandedHeightMin = null;
        public InputBoxDouble ibdLandedHeightMax = null;

        public InputBoxDouble ibdFinalPosition = null;
        public InputBoxDouble ibdSoftlandPosition = null;
        public InputBoxDouble ibdThreadDepth = null;
        public InputBoxDouble ibdPushForce = null;

        public InputBoxDouble ibdTuningLinearP = null;
        public InputBoxDouble ibdTuningLinearI = null;
        public InputBoxDouble ibdTuningLinearD = null;

        public InputBoxDouble ibdTuningRotaryP = null;
        public InputBoxDouble ibdTuningRotaryI = null;
        public InputBoxDouble ibdTuningRotaryD = null;

        public InputBoxDouble ibdSoftlandSpeed = null;
        public InputBoxDouble ibdSpeed = null;

        public InputBoxDouble ibdSoftlandSensitivity = null;
        public InputBoxDouble ibdSoftlandSensitivity2 = null;
        public InputBoxDouble ibdSoftLandForce = null;
        public InputBoxDouble ibdSetResolution = null;

        public InputBoxDouble ibdParkPosition = null;
       
        public InputBoxDouble ibdPassFailLimits = null;
        public InputBoxDouble ibdNumofThreads = null;

        
        public InputBoxDouble ibdReleaseDwellTime = null;
        public InputBoxDouble ibdLinearDistanceMin = null;

        public InputBoxDouble ibdLinearDistanceMax = null;
        public InputBoxDouble ibdRotaryDistanceMin = null;
        public InputBoxDouble ibdRotaryDistanceMax = null;
        public InputBoxDouble ibdDwellTime = null;
        public InputBoxDouble ibdDisplacement = null;
        public InputBoxDouble ibdMaxDispLim = null;
        public InputBoxDouble ibdSoftlandForceOffsetTeach = null;


        private LEAKTESTPROGRAM masterProgram = new LEAKTESTPROGRAM();

        public Actuator actuator = new Actuator();

        private ValueChecker checkerFile = new ValueChecker();

        private ValueChecker checkerLoad = new ValueChecker();

        public MessageBoxCaptions mbCaptions = new MessageBoxCaptions();


        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
            

#if SHOW_LISTBOX1
            listBox1.Visible = true;
#else
            listBox1.Visible = false;
#endif

#if SHOW_TEXTBOX2
            textBox2.Visible = true;
#else
            textBox2.Visible = false;
#endif

            //
            // the code below is necessary to make the background color of the label transparent in run tab and setup tab:
            //






            //cmbController.Items.Add(LAC25); //02/06::VIGNESH::Use this to include LAC25 controller in the program, everything else is setup automatically
            cmbController.Items.Add(LAC1);
            cmbController.Text = cmbController.Items[0].ToString();


            //logitems.Add(new logitem(chart1.Series[0]));
            lblLinearDistance_Setup.Text = "";
            chkCheckLandedDistance.Checked = true;
            

            Globals.mainForm = this;

            //            txtTerminal.Text = DateTime.Now.ToString();
            //            txtTerminal.AppendText(Environment.NewLine +"Appended");
            //            MessageBox.Show(Environment.CurrentDirectory);
            Text = programName;
            try
            {
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.ini*", SearchOption.AllDirectories))
                {
                    cmbActuator.Items.Add(Path.GetFileNameWithoutExtension(s).Replace('!', ':'));
                }
            }
            catch
            {
                MessageBox.Show("No actuator definitions found." + Environment.NewLine +
                                "Re-install the application and then try again.",
                                "Fatal error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                Application.Exit();
            }

            //            MessageBox.Show("Actuators " + cmbActuator.Items.Count.ToString());
            cmbActuator.SelectedIndex = 0;

            cmbLeakTestType.Items.Clear();  
            cmbLeakTestType.Items.Add(Globals.SOFTLAND_AND_PUSH_VEL_MODE);
            cmbLeakTestType.Items.Add(Globals.SOFTLAND_AND_PUSH_FOR_MODE);
            cmbLeakTestType.SelectedIndex = 0;


            AutoConnectComport();
#if false
                string s = "";
                s += (mbCaptions.Abort + Environment.NewLine);
                s += (mbCaptions.Cancel + Environment.NewLine);
                s += (mbCaptions.Close + Environment.NewLine);
                s += (mbCaptions.Cont + Environment.NewLine);
                s += (mbCaptions.Help + Environment.NewLine);
                s += (mbCaptions.Ignore + Environment.NewLine);
                s += (mbCaptions.No + Environment.NewLine);
                s += (mbCaptions.Ok + Environment.NewLine);
                s += (mbCaptions.Retry + Environment.NewLine);
                s += (mbCaptions.TryAgain + Environment.NewLine);
                s += (mbCaptions.Yes + Environment.NewLine);
                MessageBox.Show(s);
#endif
            // The code below is used to set the default value, maximum and minimum value to the parameters in the front end text boxes in the GUI

            ibdPushForce = new InputBoxDouble(txtPushForce, 10, 200, 100, "Push force"); // 12/20: Vignesh

            ibdTuningLinearP = new InputBoxDouble(txtTuningLinearP, 10, 1000, 100, "Linear tuning P factor");
            ibdTuningLinearI = new InputBoxDouble(txtTuningLinearI, 10, 1000, 100, "Linear tuning I factor");
            ibdTuningLinearD = new InputBoxDouble(txtTuningLinearD, 10, 1000, 100, "Linear tuning D factor");

           
            
           

            ibdSoftlandSpeed = new InputBoxDouble(txtSoftlandSpeed, 10, 100, 50, "Softland Speed");
            ibdSpeed = new InputBoxDouble(txtSpeed, 10, 100, 50, "Speed");

            ibdSoftlandSensitivity = new InputBoxDouble(txtSoftlandSensitivity, 10, 2500, actuator.variables.GetValue("VM_SOFTLAND_SENSITIVITY"), "softland Sensitivity");
            

            
            ibdSoftlandSensitivity2 = new InputBoxDouble(txtSoftlandSensitivity2, 10, 2500, actuator.variables.GetValue("FM_SOFTLAND_SENSITIVITY"), "softland Sensitivity");
            
           

            ibdSoftLandForce = new InputBoxDouble(txtSoftLandForce, 0, 100, 50, "Softland Force"); 

            ibdNumofThreads = new InputBoxDouble(txtMaxDisplacement, 0, 100, 1, "Max Displacement");
            ibdDwellTime = new InputBoxDouble(txtDwellTime, 0, actuator.LinearStroke(), 250, "Dwell Time");
            ibdFinalPosition = new InputBoxDouble(txtFinalPosition, 0, (int)(actuator.LinearStroke()), 0,  "Final Position");
            ibdDisplacement = new InputBoxDouble(txtDisplacement, 0, 100, 1, "Displacement");
            ibdSoftlandPosition = new InputBoxDouble(txtSoftlandPosition, 0, 0, 0, "Softland Position");
            ibdPositonCloseToContainer = new InputBoxDouble(txtPositionCloseToContainer, 0, 100, 100, "Position close to container");
            ibdParkPosition = new InputBoxDouble(txtParkPosition, 0, actuator.LinearStroke() - 5, 0 , "Park Position"); // 02/01 :: vignesh:: This is added to display the Park Position value as 5- Linear stroke as maximum limit
            ibdMaxDispLim = new InputBoxDouble(txtMaxDisplacement, 0, actuator.LinearMaxDispLim(), 0, "Max Displacement Limit in mm");   // 02/01 :: VIGNESH ::  




            load_values_from_actuator_ini();

            chart1.Series[0].Points.Clear();
            for (int i = -100; i <= 100; i += 5)
            {
                chart1.Series[0].Points.AddXY(i, i);
            }
//            tabControl1.SelectedIndex = 1;

            fileName = Properties.Settings.Default.File;

            if ((fileName != "") && File.Exists(fileName))
            {
                //                MessageBox.Show("Reading Settings from " + fileName);
                LoadSettingsFromFile(fileName);
            }
            else
            {
                fileName = "";
                checkerFile.Load();

            }
            EnableInputFields();
            PopulateTooltips();
            ShowSettupButtons();
        }
        // The below code is to set the default value, maximum and minimum value for the parameters in the front end GUI
        public void load_values_from_actuator_ini()
        {
            ibdPositonCloseToContainer = new InputBoxDouble(txtPositionCloseToContainer, 0, (int)(actuator.LinearStroke() - 5.0), 0, "Position Close To Container [mm]");
            

            ibdLandedHeightMin = new InputBoxDouble(txtLandedDistanceMin, 0, (int)(actuator.LinearStroke())-5, 0, "Landed Height Minimum [mm]");
           

            ibdLandedHeightMax = new InputBoxDouble(txtLandedDistanceMax, 0, (int)(actuator.LinearStroke() - 5), 0 , "Landed Height Maximum [mm]");
            

            
            ibdFinalPosition = new InputBoxDouble(txtFinalPosition, 0, (int)(actuator.LinearStroke()), 0, "Final Position [mm]");
           

            ibdSoftlandPosition = new InputBoxDouble(txtSoftlandPosition, 0, (int)(actuator.LinearStroke()), 0, "Softland Position [mm]");

           
            ibdDisplacement = new InputBoxDouble(txtDisplacement, 0, 200, (ibdFinalPosition.Doublevalue - ibdSoftlandPosition.Doublevalue), "Displacement [mm]");
            txtDisplacement.Text = (ibdFinalPosition.Doublevalue - ibdSoftlandPosition.Doublevalue).ToString();


            ibdParkPosition = new InputBoxDouble(txtParkPosition, 0, actuator.LinearStroke(), 0, "Park Position [mm]");
            txtParkPosition.Text = ibdParkPosition.Doublevalue.ToString();

            ibdDwellTime = new InputBoxDouble(txtDwellTime, 0, actuator.LinearDwellTime(), 250, "Dwell Time");
            
            ibdMaxDispLim = new InputBoxDouble(txtMaxDisplacement, 0, (int)(actuator.LinearMaxDispLim()), 0 , "Max Displacement Limit [mm]");

        }


        // The below code is to connect the COM port automatically after opening the GUI
        private void AutoConnectComport()
        {
            FillPortnames();
            switch (cmbPort.Items.Count)
            {
                case 0:
                    break;
                case 1:
                    cmbPort.SelectedIndex = 0;
                    Opencomport();
                    break;
                default: // More ports: Open the last selected one or let the user select one
                    for (int i = 0; i < cmbPort.Items.Count; i++)
                    {
                        if (Properties.Settings.Default.Port == cmbPort.Items[i].ToString())
                        {
                            cmbPort.SelectedIndex = i;
                            Opencomport();
                            return;
                        }
                    }
                    cmbPort.SelectedIndex = 0;
                    MessageBox.Show("Multiple COM ports found, can not decide which one to use." + Environment.NewLine + Environment.NewLine +
                                    "Please select a COM Port first and then hit the Connect button", "Automatic port selection not possible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
            SetButtons(port.IsOpen());
        }
     // public List<logitem> logitems = new List<logitem>();

        public void DefineToolTip(ToolTip tooltip, Control control, String text)
        {
            tooltip.ShowAlways = true;
            tooltip.UseFading = true;
            tooltip.UseAnimation = true;
            tooltip.ShowAlways = true;
            int autoPopdelay = 50 * text.Length;
            if (autoPopdelay < 5000)
            {
                autoPopdelay = 5000;
            }
            if (autoPopdelay > 30000)
            {
                autoPopdelay = 30000;
            }
            tooltip.AutoPopDelay = autoPopdelay;
            tooltip.InitialDelay = 500;
            tooltip.IsBalloon = false;
            tooltip.SetToolTip(control, text);
        }

       
        // The below code is to display messages when the mouse over the text box in the front end GUI
        public void PopulateTooltips()
        {
            const string tiptextSaveChart =
                 "Saves the last log to a file in .csv format.";
            DefineToolTip(new ToolTip(), btnSaveChart, tiptextSaveChart);

            const string tiptextActuator =
                "Select the right actuator here";
            DefineToolTip(new ToolTip(), cmbActuator, tiptextActuator);
            DefineToolTip(new ToolTip(), lblActuator, tiptextActuator);

            const string tiptextDisplacement =
               "Dispalcement is the value that the SMAC actuator moved from the softlanded position to the final position on the container.\n" +
                "This value can be used to relate the quality of the container for leak testing application";
            DefineToolTip(new ToolTip(), txtDisplacement, tiptextDisplacement);
            DefineToolTip(new ToolTip(), lblDisplacement, tiptextDisplacement);
            DefineToolTip(new ToolTip(), lblDisplacementSetup1, tiptextDisplacement);
            DefineToolTip(new ToolTip(), lblDisplacementRun1, tiptextDisplacement); 

            const string tiptextPort = 
                "Select serial communication port";
            DefineToolTip(new ToolTip(), cmbPort, tiptextPort);
            DefineToolTip(new ToolTip(), lblPort, tiptextPort);

            const string tiptextPositionCloseToPart = 
                "Rapid traverse position close to the container surface.\n" +
                "Please note: This is pre-calculated in the \"Teach\" process, but can be adjusted if desired.";
            DefineToolTip(new ToolTip(), txtPositionCloseToContainer, tiptextPositionCloseToPart);
            DefineToolTip(new ToolTip(), lblPositionCloseToPart, tiptextPositionCloseToPart);

            const string tipMaxDispLim =
                "This is the maximum displacement that the selected actuator can be setup for specific stroke \n" +
                "Please note: This is pre-calculated (5 - Stroke of the selected actuator) and has a limit, but can be adjusted if desired within limit.";
            DefineToolTip(new ToolTip(), txtMaxDisplacement, tipMaxDispLim);
            DefineToolTip(new ToolTip(), lblMaxDispLim, tipMaxDispLim);

            DefineToolTip(new ToolTip(), btnTeach, 
                "Use this once the Actuator and the controller selections have been made and the program has been saved into the controller.\n" + 
                "This will teach the actuator where the Position close to the container is at as well as the linear force settings required.\n" +
                "This will allow the actuator to learn the forces & locations at various points throughout the path of the leak testing process.\n"+
                "These values will also be used to preset some suggested starting values( Position close to part, Landed Distance,etc) for other fields in the GUI.\n"+  
                "Another \"Save to Controller\" is required after the teach is completed in order to update these settings.");

            const string tiptextPartLocationMin =
                "This is the minimum allowable position that is close to the container.\n" +
                "This limit is pre-calculated in the “Teach” process, but can be adjusted if desired";
            DefineToolTip(new ToolTip(), txtLandedDistanceMin, tiptextPartLocationMin);
            DefineToolTip(new ToolTip(), lblLandedDistanceMin, tiptextPartLocationMin);

            const string tiptextPartLocationMax =
                "This is the maximum allowable position close to container.\n" +
                "This limit is pre-calculated in the “Teach” process, but can be adjusted if desired";
            DefineToolTip(new ToolTip(), txtLandedDistanceMax, tiptextPartLocationMax);
            DefineToolTip(new ToolTip(), lblLandedDistanceMax, tiptextPartLocationMax);
            
            const string tiptextSoftLandForce = 
                "Enter the maximum percentage of force for the SMAC to use during the Softland on the container.\n" +
                "Note 1: The SMAC Leak Test Center handles this value and calculate softanding force based on it. \n" +
                "Note 2: This setting in combination with the Sensitivity and Speed settings will affect the functionality of the SMAC actuator.";
            DefineToolTip(new ToolTip(), txtSoftLandForce, tiptextSoftLandForce);
            DefineToolTip(new ToolTip(), lblSoftLandForce, tiptextSoftLandForce);

            const string tiptextSoftlandSensitivity =
                "Enter the sensitivity setting.\n" +
                "This should be 5 to 20 counts over what is seen with a good condition container\n";
                
            DefineToolTip(new ToolTip(), txtSoftlandSensitivity, tiptextSoftlandSensitivity);
            DefineToolTip(new ToolTip(), lblSoftlandSensitivity, tiptextSoftlandSensitivity);
            {
                const string tiptextPushForce =
                  "Enter the maximum percentage of Push Force for the SMAC actuator to use during FORCE MODE.";
                DefineToolTip(new ToolTip(), txtPushForce, tiptextPushForce);
                DefineToolTip(new ToolTip(), lblPushForce, tiptextPushForce);
            }


            {
                const string tiptextLinearPID = 
                    "Use this to adjust the PID settings for the linear axis only if needed (Example: Excessive tooling weight)";
                DefineToolTip(new ToolTip(), gbxTuningLinear, tiptextLinearPID);
                DefineToolTip(new ToolTip(), txtTuningLinearP, tiptextLinearPID);
                DefineToolTip(new ToolTip(), txtTuningLinearI, tiptextLinearPID);
                DefineToolTip(new ToolTip(), txtTuningLinearD, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearP, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearI, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearD, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearPUnits, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearIUnits, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearDUnits, tiptextLinearPID);
            }
            {
                const string tiptextRotaryPID = 
                    "Use this to adjust the PID settings for the rotary axis only if needed (Example: Excessive tooling weight)";
                DefineToolTip(new ToolTip(), gbxTuningRotary, tiptextRotaryPID);
               
                
                
               
                
               
                
                
                
            }

            {
                const string tiptextTeacResult = 
                    "This is information that was found during the “Teach” routine";
                DefineToolTip(new ToolTip(), gbxTeachResult, tiptextTeacResult);
                DefineToolTip(new ToolTip(), txtTeachedSqLinearHome, tiptextTeacResult);
                DefineToolTip(new ToolTip(), lblTeachedSqLinearHome, tiptextTeacResult);
                DefineToolTip(new ToolTip(), txtTeachedSqLinear, tiptextTeacResult);
                DefineToolTip(new ToolTip(), lblTeachedSqLinear, tiptextTeacResult);
                DefineToolTip(new ToolTip(), txtTeachedLandedDis, tiptextTeacResult);
                DefineToolTip(new ToolTip(), lblTeachedLandedDis, tiptextTeacResult);
                DefineToolTip(new ToolTip(), lblTeachedHeightUnits, tiptextTeacResult);
            }

            const string tiptextSpeed =
                "Enter the percentage of speed for universal speed setting apart from softland speed. \n " +
                "Note: This value will be taken by the SMAC controller and will generate other equivalent speed settings to the actuator.\n";
            DefineToolTip(new ToolTip(), txtSpeed, tiptextSpeed);
            DefineToolTip(new ToolTip(), lblSpeed, tiptextSpeed);

            const string tiptextSoftlandSpeed = 
                "Enter percentage of desired softland speed.\n" +
                "Lower settings will provide better results.\n" +
                "This setting in combination with the Sensitivity and Softland Force settings will affect the functionality of the SMAC.";

      
            DefineToolTip(new ToolTip(), txtSoftlandSpeed, tiptextSoftlandSpeed);
            DefineToolTip(new ToolTip(), lblSoftlandSpeed, tiptextSoftlandSpeed);

            DefineToolTip(new ToolTip(), btnActuatorInfo,
                "Selected actuator detailed information.");
            DefineToolTip(new ToolTip(), btnLoadSettingsFromFile, 
                "Use this to load GUI settings from file.");
            DefineToolTip(new ToolTip(), btnSaveSettingsToFile, 
                "Save GUI settings to computer");
            DefineToolTip(new ToolTip(), btnSaveProgramToFile, 
                "Save compiled LAC-25 program to computer");
            DefineToolTip(new ToolTip(), btnSaveToController, 
                "Save compiled LAC-25 program to controller");
            DefineToolTip(new ToolTip(), btnSingleCycle, 
                "Use this to run a single cycle.\n" +
                "The \"Teach\" routine should have been run at least once initially, prior to the use of this button to learn the height of the part.");
            DefineToolTip(new ToolTip(), btnRun, 
                "Use this to run the program that has been downloaded into the controller");
            DefineToolTip(new ToolTip(), btnContactInfo, 
                "Use this to get SMAC contact information");
            DefineToolTip(new ToolTip(), btnHelpDocument, 
                "Use this to open the help document.\nThis document will be opened in a separate window");
            DefineToolTip(new ToolTip(), btnStop, 
                "Use this to stop the program in the controller and to deactivate the actuator");
            DefineToolTip(new ToolTip(), btnUndo, 
                "This is the tooltip for the undo button");

            DefineToolTip(new ToolTip(), chkCheckLandedDistance, 
                "Select this option for verifying landed distance.\n" +
                "The limits are pre-calculated in the \"Teach\" process, but can be adjusted if desired. ");
            
            DefineToolTip(new ToolTip(), chkShowAll, 
                "Select this to display all of the data being sent out of the SMAC controller.");

//            DefineToolTip(new ToolTip(), txtTerminal, 
//                "This is the tooltip for the terminal window");

            DefineToolTip(new ToolTip(), chart1, 
                "This is a display of the Displacement vs. Cycle counts.\n" 
                );

            DefineToolTip(new ToolTip(), pictureBoxRun, 
                "General information for an leak test application");

            DefineToolTip(new ToolTip(), pictureBoxSetup, 
                "General information for an leak test application");
                      

            const String tiptextSoftlandPosition =
            "Softland position is the position that SMAC actuator touched the container and softlanded";
                    DefineToolTip(new ToolTip(), lblSoftlandPosition, tiptextSoftlandPosition);
                    DefineToolTip(new ToolTip(), txtSoftlandPosition, tiptextSoftlandPosition);
                 

            
          

            {
                const string tiptextParkPosition =
                    "Park position is the position for the SMAC actuator to park after teach operation, initialization and after single cycle";
                DefineToolTip(new ToolTip(), lblParkPosition, tiptextParkPosition);
                DefineToolTip(new ToolTip(), txtParkPosition, tiptextParkPosition);
            }

                       
            
                const string tiptextDwellTime =
                    "This is the amount of time that is needed for the actuator to push the part before moving to the next cycle.";
                DefineToolTip(new ToolTip(), lblDwellTime, tiptextDwellTime);
                DefineToolTip(new ToolTip(), txtDwellTime, tiptextDwellTime);
            

                      
            {
                const string tiptextLeakTestType =
                    "Select the type of leak testing operation. For further details check the Help Document: \n"; 
                    
                DefineToolTip(new ToolTip(), lblLeakTestType, tiptextLeakTestType);
                DefineToolTip(new ToolTip(), cmbLeakTestType, tiptextLeakTestType);
            }
        }

       
        private void Title()
        {
            if (fileName == "")
            {
                Text = programName;
            }
            else
            {
                Text = fileName + " - " + programName;
            }
        }

        public void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (port.IsOpen())
            {
                BeginInvoke(new SetTextDeleg(si_DataReceived), port.GetAllBytes());
            }
        }

        private void si_DataReceived(string data)
        {
            for (int i = 0; i < data.Length; i++)
            {
#if SHOW_LISTBOX1

                string print = "";
                if ((int)data[i] < 0x20)
                {
                    print = ((int)data[i]).ToString("x2");
                }
                else
                {
                    print = data[i].ToString();
                }
                listBox1.Items.Add(print);
                listBox1.TopIndex = listBox1.Items.Count - 1;
#endif

                if (checkFirst)
                {
                    if (data[i] == '!')
                    {
                        if (chkShowAll.Checked)
                        {
                            ignoreResponse = false;
                        }
                        else
                        {
                            ignoreResponse = true;
                        }
                    }
                    else
                    {
                        ignoreResponse = false;
                    }
                    checkFirst = false;
                }


              
                switch (data[i])
                {
                    case '\b':      // Backspace
                        //
                        // The backspace handling looks a bit complicated but this method is needed to assure that the cursor
                        // is at the correct position
                        //

                        txtTerminal.AppendText(" ");        // Start with appending a space character to make sure the cursor is at end of line
                        int firstIndex = txtTerminal.GetFirstCharIndexOfCurrentLine();
                        string s = txtTerminal.Text.Substring(firstIndex);
                        txtTerminal.Text = txtTerminal.Text.Remove(firstIndex);
                        switch (s.Length)
                        {
                            case 1:
                                break;
                            default:
                                s = s.Remove(s.Length - 2);
                                break;
                        }
                        txtTerminal.AppendText(s);
                        break;

                    case '\r':      // The CR character terminates a response
                        processControllerResponse();
                        if (!ignoreResponse)
                        {
                            txtTerminal.AppendText(data[i].ToString());
                        }
                        break;

                    case '\n':     // The New line character moves to the next line
                        checkFirst = true;
                        if (!ignoreResponse)
                        {
                            txtTerminal.AppendText(data[i].ToString());
                        }
                        break;
                    

                    default:        // Echo all other characters including CR and LF simply to the output text box
                        lastCommand += data[i].ToString();
                        if (!ignoreResponse)
                        {
                            txtTerminal.AppendText(data[i].ToString());
                        }
                        break;
                }
            }
        }

        // The below code is to generate a chart with displacement vs cycle counts 
        private void Plot(int dis, int error)
        {
          //  chart1.Series[0].Points.AddXY((double)dis / 1000, (int)error);    // Plot in mm
            chart1.Series[0].Points.Add(new System.Windows.Forms.DataVisualization.Charting.DataPoint((double)dis, (double)error));

        }

        //
        // Controller responses can have the following format:
        //
        // C denotes a character
        // X and Y means a integer number
        //
        // !X,Y     Where X and Y are integer numbers
        //          This is a plot point: X is the linear position in Encoder counts
        //          Y is the rotary error in encoder counts
        // !CX      C is the character that indicates the source, X is the value
        //          The following source characters are defined:
        //          S - Softland Position alias (Landed height in counts)
        //          D - Measured hole depth
        //          P - Result of Push/Pull test in encoder counts
        //          I - Initialisation:
        //          F - Final Position
        //
        //          I0 : Clear screeen at startup
        //          I1 : Measurement started: Clear chart and results
        //          I2 : Measurement finished and back at home position
        //
        //          E - Error condition
        //
        // Any other controller response will be ignored
        //



        // The below code assign val1 and val2 to the displacement and cycle counts after each sequence is complete and will plot the result in the chart 
        void processControllerResponse()
        {
            string response = lastCommand.Trim();

            lastCommand = "";

#if SHOW_TEXTBOX2

            textBox2.AppendText(response + "\r\n");
#endif

            int val1 = 0;
            int val2 = 0;
            Color color = Color.Black;



            if (response.Length == 0)           // ignore empty lines
            {
                return;
            }

            if (response[0] != '!')            // This is the command prompt: ignore
            {
                return;
            }

            response = response.Substring(1);     // Delete the !
            response = response.Trim();

            if (response.IndexOf(',') > 1)     // Then this could be a plot command consisting of 2 comma separated integers
            {
                string[] values = response.Split(',');
                
                if (values.Count() == 2)
                {
                    if (int.TryParse(values[0], out val1) && int.TryParse(values[1], out val2))
                    {
                    
                        Plot(val2, val1 / 1000);
                        return;
                    }
                }
            }

            // The below code interprets the result of each program sequence and assign them to val1
            else if ((response.Length >= 2) && ("FHPEDISMTLQR".IndexOf(response[0]) >= 0) && int.TryParse(response.Substring(1), out val1))
            {
                
                switch (response[0])
                {
                    case 'L':   // Landed height for teach function
                        double landedDistance = millimeters(val1);
                       
                        ibdPositonCloseToContainer.ReadValue();
                        ibdLandedHeightMin.ReadValue();
                        ibdLandedHeightMax.ReadValue();
                        txtTeachedLandedDis.Text = Utils.neutralDouble(landedDistance);
                        break;

                    case 'M':
                        double Teach2Distance = millimeters(val1);
                        txtPositionCloseToContainer.Text = (Teach2Distance - 10).ToString();
                        txtLandedDistanceMin.Text = (Teach2Distance - 2).ToString();
                        txtLandedDistanceMax.Text = (Teach2Distance + 2).ToString();
                        txtTeached2LandedDis.Text = Utils.neutralDouble(Teach2Distance);
                        break;
                        
                    case 'F':// 01/5 vignesh :: This is to display Final position result
                        double FinalPosition = millimeters(val1)/actuator.LinearResoloution();
                        txtFinalPosition.Text = FinalPosition.ToString();
                        break; 
                    case 'Q':
                        txtTeachedSqLinear.Text = val1.ToString();
                        // This is the first output from the teach function
                        // Show a messagebox to present the teach result.
                        //
                        string message =  "Step1 Teach succeded." + Environment.NewLine;
                        message += Environment.NewLine;
                        message += "Measured Distance = " + txtTeachedLandedDis.Text + Environment.NewLine;
                        message += "Measured SQ close to part = " + txtTeachedSqLinear.Text + Environment.NewLine;
                        message += "Measured SQ at home position = " + txtTeachedSqLinearHome.Text;
                        MessageBox.Show(message, "Step1 Teach Result", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        port.DisableHandler();
                        if (port.AbortAndWaitForPrompt())
                        {
                            if (MessageBox.Show("Place a good object in the test area","", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                            {

                                btnUndo1_Click(null, null);
                                port.WriteString("MS246\r");
                                
                            } 


                        }
                        port.EnableHandler();
                        break;

                    case 'T':
                        txtTeached2SqLinear.Text = val1.ToString();

                        //This is the second output from teach function

                        string message2 = "Step2:Teach Succeeded." + Environment.NewLine;
                        message2 += Environment.NewLine;
                        message2 += "Measured Teach 2 Distance =" + txtTeached2LandedDis.Text + Environment.NewLine;
                        message2 += "Measured Teach 2 SQ at home position =" + txtTeached2SqLinear.Text + Environment.NewLine; 
                        MessageBox.Show(message2, "Step2 Teach Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;

                    case 'R':
                        txtTeachedSqLinearHome.Text = val1.ToString();
                        break;
                    case 'S':   // 12/21: Vignesh-  Softland Position alias LandedHeight2 
                        double SoftlandPosition = millimeters(val1)/actuator.LinearResoloution();
                        txtSoftlandPosition.Text = SoftlandPosition.ToString();
                        ibdSoftlandPosition.ReadValue();
                          
                        break;            
                    case 'E':
                        return;
                    case 'D':  // 01/25 VIGNESH :: This is to add Displacment
                        double Displacement = millimeters(val1)/actuator.LinearResoloution() ;
                        txtDisplacement.Text = Displacement.ToString();
                        ibdDisplacement.ReadValue();
                        lblDisplacementRun1.Text = lblDisplacementSetup1.Text = Displacement.ToString() + " mm";
                                                
                            if (Displacement >= ibdMaxDispLim.Doublevalue) 
                            {
                                color = Color.Red;
                            }
                            else
                            {
                                color = Color.Lime;
                            }
                        
                        lblDisplacementRun1.BackColor = lblDisplacementSetup1.BackColor = color;
                      
                        return;

                    case 'I':   // Initialisations:
                        switch (val1)
                        {
                            case 0:
                                txtTerminal.Text = "";
                                chart1.Series[0].Points.Clear();
                                return;
                            case 1:
                                txtTerminal.Text = "";
                                

                                lblLinearDistance_Setup.Text = "";                              
                                return;
                            case 2:
                                return;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        private void Opencomport()
        {
            FillPortnames();
            if (port.IsOpen())
            {
                port.Close();
            }
            if (cmbPort.SelectedItem != null)
            {
                port.Open(cmbPort.SelectedItem.ToString(), 57600, sp_DataReceived);
                if (!port.IsOpen())
                {
                    cmbPort.SelectedItem = null;
                }
            }
            SetButtons(port.IsOpen());
        }


        private void btnClearcontrolleroutputwindow_Click(object sender, EventArgs e)
        {
            txtTerminal.Clear();
            txtTerminal.Focus();
        }


        private void txtTerminal_KeyPress(object sender, KeyPressEventArgs e)
        {
            //            txtDebug.AppendText("Character " + e.KeyChar + Environment.NewLine);
            if (LoggedIn())
            {
                switch ((int)e.KeyChar)
                {
                    case 1:            // Ctrl-A
                        txtTerminal.SelectAll();
                        break;
                    case 3:             // Ctrl-C
                        Clipboard.SetText(txtTerminal.SelectedText);
                        break;
                    default:
                        port.WriteString(e.KeyChar.ToString());
                        break;
                }
            }
            else
            {
                MessageBox.Show("Typing in terminal window not allowed", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            e.Handled = true;
        }

        //
        // FillPortnames will compare the contents of the port combo box with the actual serial ports
        // that are present in the system.
        //
        // First a check is done if all actual ports are in the combobox. Ports that are not present are added to the combobox.
        // Then a check is done if all combobox ports are present in the system. Missing ports are removed from the combobox.
               

        private bool FillPortnames()
        {
            List<string> portnames = new List<string>(SerialPort.GetPortNames());
            List<string> cmbItems = new List<string>();
            for (int i = 0; i < cmbPort.Items.Count; i++)
            {
                cmbItems.Add(cmbPort.Items[i].ToString());
            }

            foreach (string s in portnames)
            {
                if (!cmbItems.Contains(s))
                {
                    cmbPort.Items.Add(s);
                    cmbItems.Add(s);
                }
            }

            foreach (string s in cmbItems)
            {
                if (!portnames.Contains(s))
                {
                    cmbPort.Items.Remove(s);
                }
            }

            if (cmbPort.Items.Count == 0)
            {
                MessageBox.Show("There is no serial port detected on your system." + Environment.NewLine + Environment.NewLine +
                    "It will not be possible to connect to a controlller.", "No serial port found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                port.Close();
                SetButtons(false);
            }


            return (cmbPort.Items.Count > 0);
        }


        // check if the user has entered reasonable values that are conform
        // the actuator specification.
        // The index position is 2-4 mm from the full retract position
        // The stroke is defined as full stroke of teh actuator.
        // This function will show a warning message
        // return value is true if the input is ok
        // false if the input is not ok and the user has selected cancel
        // 

        private bool CheckUserInput()
        {
            double maxLinear = actuator.LinearStroke() - 4.0;

            double maxperm = actuator.LinearStroke() - 5;  

            string message = "";
            //if ((ibdPositonCloseToContainer.Doublevalue + ibdThreadDepth.Doublevalue) > maxLinear)
            //{
            //    message = "The sum of position close to part (" + ibdPositonCloseToContainer.Stringvalue + " mm) and thread depth (" + ibdThreadDepth.Stringvalue + " mm)" + Environment.NewLine +
            //              "must be within the stroke range of the actuator (" + Utils.neutralDouble(maxLinear) + " mm)";
            //}
            if ((ibdPositonCloseToContainer.Doublevalue) > maxLinear)
            {
                message = "The Position close to Container (" + ibdPositonCloseToContainer.Stringvalue + " mm)"  + Environment.NewLine +
                          "must be within the stroke range of the actuator (" + Utils.neutralDouble(maxLinear) + " mm)";
            }
            else if (chkCheckLandedDistance.Checked && (ibdLandedHeightMin.Doublevalue < ibdPositonCloseToContainer.Doublevalue))
            {
                message = "The Min value for landed height check (" + ibdLandedHeightMin.Stringvalue + " mm) must be higher than the position close to part (" + ibdPositonCloseToContainer.Stringvalue + " mm)";
            }

            else if (chkCheckLandedDistance.Checked && (ibdLandedHeightMax.Doublevalue < ibdPositonCloseToContainer.Doublevalue))
            {
                message = "The Max value for landed height check (" + ibdLandedHeightMax.Stringvalue + " mm) must be higher than the position close to part (" + ibdPositonCloseToContainer.Stringvalue + " mm)";
            }

            else if (chkCheckLandedDistance.Checked && (ibdLandedHeightMax.Doublevalue < ibdLandedHeightMin.Doublevalue))
            {
                message = "The Max value for landed height check (" + ibdLandedHeightMax.Stringvalue + " mm) must be higher than The Min value for landed height check (" + ibdLandedHeightMin.Stringvalue + " mm)";
            }

            //else if (chkCheckLandedDistance.Checked && ((ibdLandedHeightMax.Doublevalue + ibdThreadDepth.Doublevalue) > maxLinear))
            //{
            //    message = "The sum of Max value for landed height check (" + ibdLandedHeightMax.Stringvalue + " mm) and thread depth (" + ibdThreadDepth.Stringvalue + " mm)" + Environment.NewLine +
            //              "must be within the stroke range of the actuator (" + Utils.neutralDouble(maxLinear) + " mm)";
            //}

            else if (chkCheckLandedDistance.Checked && ((ibdLandedHeightMax.Doublevalue) > maxLinear))
            {
                message = "The Max value for landed height check (" + ibdLandedHeightMax.Stringvalue + " mm)" + Environment.NewLine +
                          "must be within the stroke range of the actuator (" + Utils.neutralDouble(maxLinear) + " mm)";
            }

            //else if (chkCheckThreadClearance.Checked && rbnTop.Checked && ibdThreadDepth.Doublevalue < (4.0 * GetPitch()))
            //{
            //    message = "Checking the thread clearance \"At top\" is not possible with a thread depth that is less than 4 times the pitch" +Environment.NewLine +
            //              "Select the \"At bottom\" option or increase the thread depth to at least " + (4.0 * GetPitch()).ToString() + " mm.";

            //}

            if (message != "")
            {
                return DialogResult.OK == MessageBox.Show(message + Environment.NewLine + Environment.NewLine +
                                          "Ignoring this message may result in a non functioning program in the controller" + Environment.NewLine  + Environment.NewLine +
                                          "Select OK to continue and to ignore this warning." + Environment.NewLine +
                                          "Select Cancel if you want to correct your input first", "User input out of limits", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
            return true;
        }


        private void btnActuatorinfo_Click(object sender, EventArgs e)
        {
            string strTemp = "";
            foreach (string s in actuator.Info())
            {
                strTemp += (s + Environment.NewLine);
            }

            MessageBox.Show(strTemp, "Actuator info",MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        
     // The below code is to save the program to file and the parameters will be saved in the file in following format

        private void Compile()
        {

            // First create the header
            //
            header.Clear();
            header.Add(";");
            header.Add("; Program automatic created by: " + programName);
            header.Add("; Creation time : " + DateTime.Now.ToString());
            header.Add(";");
            header.Add("; Actuator info:");

            foreach (string s in actuator.Info())
            {
                if (s != "")
                {
                    header.Add("; " + s);
                }
            }
            header.Add(";");
            
            header.Add("; Position close to container = " + ibdPositonCloseToContainer.Stringvalue + " mm.");
            if (chkCheckLandedDistance.Checked)
            {
                header.Add("; Check landed height min = " + ibdLandedHeightMin.Stringvalue + " mm.");
                header.Add("; Check landed height max = " + ibdLandedHeightMax.Stringvalue + " mm.");
            }
            else
            {
                header.Add("; Landed height not checked");
            }
         
            header.Add("; Push Force = " + ibdPushForce.Stringvalue + " %");
            header.Add("; LeakTest Type =" + cmbLeakTestType.SelectedItem.ToString()); 

            header.Add(";");
            header.Add("; Linear PID settings : P=" + ibdTuningLinearP.Stringvalue + "%, " +
                                               "I=" + ibdTuningLinearI.Stringvalue + "%, " +
                                               "D=" + ibdTuningLinearD.Stringvalue + "%.");

             

            header.Add("; Softland Speed = " + ibdSoftlandSpeed.Stringvalue + "%");
            header.Add("; Softland sensitivity velocity Mode = " + ibdSoftlandSensitivity.Stringvalue + " counts");
            header.Add("; Softland sensitivity force Mode = " + ibdSoftlandSensitivity2.Stringvalue + " counts");
            header.Add("; Dwell Time = " + ibdDwellTime.Stringvalue + " ms");
            header.Add("; Park Position = " + ibdParkPosition.Stringvalue + "mm"); // :: Vignesh : 01/19
          
            header.Add("; Max Disp Limit =" + ibdMaxDispLim.Stringvalue + "mm");
            header.Add("; Softland Position =" + ibdSoftlandPosition + "mm"); 
            header.Add("; Displacement =" + ibdDisplacement + "mm"); 
            

            if (cmbLeakTestType.SelectedIndex == 1)
            {
                header.Add(";Push force =" +
                  ibdPushForce.Stringvalue + " mm");
               
            }
            else
            {
                header.Add(";not input Push force");
            }

            if (cmbLeakTestType.SelectedIndex == 2)
            {
                header.Add(";Push force  =" +
                  ibdPushForce.Stringvalue + " mm");
            }
            else
            {
                header.Add(";not input Push force");
            }

            string strTemp = "";
            foreach (string s in header)
            {
                strTemp += (s + Environment.NewLine);
            }
//            MessageBox.Show(strTemp, "Header");


            // the below code is to extract the user entered values from the text box in the front end and use it for calculations in the back ground
            actuator.Load(cmbActuator.SelectedItem.ToString());
            masterProgram.variables = actuator.variables;
            masterProgram.ResetTranslation();          

            masterProgram.SetLinearPID(ibdTuningLinearP.Doublevalue, ibdTuningLinearI.Doublevalue, ibdTuningLinearD.Doublevalue);
            masterProgram.SetSoftlandSpeed(ibdSoftlandSpeed.Doublevalue / 100.0);
            masterProgram.SetSpeed(ibdSpeed.Doublevalue / 100.0);
            masterProgram.SetResolution(actuator.LinearResoloution());


            masterProgram.SetSoftlandSensitivity(ibdSoftlandSensitivity.Doublevalue);
            masterProgram.SetSoftlandSensitivity2(ibdSoftlandSensitivity2.Doublevalue);
           


            masterProgram.SetPositionCloseToContainer(LinearEncoderCounts(ibdPositonCloseToContainer.Doublevalue));
            masterProgram.SetLandedHeightMin(LinearEncoderCounts(ibdLandedHeightMin.Doublevalue));
            masterProgram.SetLandedHeightMax(LinearEncoderCounts(ibdLandedHeightMax.Doublevalue));


            masterProgram.SetParkPosition((ibdParkPosition.Doublevalue) /(actuator.LinearResoloution())*1000);
            masterProgram.SetPushForce(ibdPushForce.Doublevalue / 100.0);
            masterProgram.SetSoftLandForce(ibdSoftLandForce.Doublevalue / 100.0);
            masterProgram.SetDwellTime(LinearEncoderCounts(ibdDwellTime.Doublevalue * 5) / 1000);
            masterProgram.SetParkPositionAdd(((ibdParkPosition.Doublevalue + 2)*1000)*(200)/(5*40* actuator.LinearResoloution())); 

            masterProgram.SetFinalPosition(LinearEncoderCounts((ibdFinalPosition.Doublevalue) / actuator.LinearResoloution()));
            masterProgram.SetMaxDispLim(LinearEncoderCounts(ibdMaxDispLim.Doublevalue));

            masterProgram.SetDisplacement(LinearEncoderCounts(ibdDisplacement.Doublevalue));
            masterProgram.SetMaxPermissableStroke(LinearEncoderCounts(((int)actuator.LinearStroke() - 5)*(10)/actuator.LinearResoloution()));  // Vignesh ::01/23  
          
            masterProgram.SetSoftlandPosition(LinearEncoderCounts((ibdSoftlandPosition.Doublevalue)/actuator.LinearResoloution()));

            masterProgram.EnableCheckLandedDistance(chkCheckLandedDistance.Checked);
           
            masterProgram.SetLeakTestType(cmbLeakTestType.SelectedItem.ToString());
            masterProgram.CheckMovementSelected(cmbLeakTestType.SelectedIndex);










            masterProgram.Create();
        }

        // The below code is to save the program to file and the program will be extracted from the master program( leak test program) 

        private void btnSaveProgramToFile_Click(object sender, EventArgs e)
        {
            if (CheckUserInput() == false)
            {
                return;
            }

            Compile();
            SaveFileDialog filedialog = new SaveFileDialog();
            filedialog.Filter = "Text files (*.txt)|*.txt";
            filedialog.DefaultExt = "txt";
            filedialog.FileName = Path.GetFileNameWithoutExtension(fileName) + "." + filedialog.DefaultExt;
            string chosenFile = "";

            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                chosenFile = filedialog.FileName;
                System.IO.StreamWriter outputfile = new System.IO.StreamWriter(chosenFile);

                foreach (string s in header)
                {
                    outputfile.WriteLine(s);
                }
                for (int i = 0; i < masterProgram.Count(); i++)
                {
                    outputfile.WriteLine(masterProgram.GetLine(i));
                }
                outputfile.Close();
            }
        }

        // The below code is to save all the program into the controller

        private void btnSaveInController_Click(object sender, EventArgs e)
        {
            if (CheckUserInput() == false)
            {
                return;
            }

            Compile();

            port.DisableHandler();

            if (port.AbortAndWaitForPrompt() == false)
            {
            }
            else
            {
                btnRun.Enabled = false;
                btnSingleCycle.Enabled = false;
                btnStop.Enabled = false;

                port.Mode9600(true);
                for (int i = 0; i < masterProgram.Count(); i++)
                {
                    if (masterProgram.GetLine(i)[0] != ';')
                    {
                        port.WriteStringAndWaitForPrompt(masterProgram.GetLine(i) + "\r");
                    }
                }
                port.Mode9600(false);
            }
            port.EnableHandler();
            btnRun.Enabled = true;
            btnSingleCycle.Enabled = true;
            btnStop.Enabled = true;

        }


        private void txtPositionCloseToContainer_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPositionCloseToContainer_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtMaxDispLim_Leave(object sender, EventArgs e)
        {
            ibdMaxDispLim.ReadValue();     // Read and check the value
        }

        private void txtMaxDispLim_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtMaxDispLim_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }


        private void txtPositionCloseToContainer_Leave(object sender, EventArgs e)
        {
            ibdPositonCloseToContainer.ReadValue();     // Read and check the value
        }

      /*  private void EnableLogItems(bool enable)
        {
            foreach (logitem item in logitems)
            {
                item.Enable(enable);
            }
            
            btnExport.Enabled = enable;
            
        }*/



        private void txtSetResolution_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSetResolution_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSetResolution_Leave(object sender, EventArgs e)
        {
            ibdSetResolution.ReadValue();     // Read and check the value
        }


        private void txtPartLocationMin_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPartLocationMin_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtPartLocationMin_Leave(object sender, EventArgs e)
        {
            ibdLandedHeightMin.ReadValue();     // Read and check the value
        }



        private void txtSoftlandSensitivity2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSoftlandSensitivity2_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

       private void txtSoftlandSensitivity2_Leave(object sender, EventArgs e)
        {
            ibdSoftlandSensitivity2.ReadValue();     // Read and check the value
        }

        private void txtPartLocationMax_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPartLocationMax_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }
              
        private void txtPartLocationMax_Leave(object sender, EventArgs e)
        {
            ibdLandedHeightMax.ReadValue();     // Read and check the value
        }
        
        private void txtPushForce_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPushForce_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtPushForce_Leave(object sender, EventArgs e)
        {
            ibdPushForce.ReadValue();     // Read and check the value
        }

        private void txtSoftLandForce_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSoftLandForce_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSoftLandForce_Leave(object sender, EventArgs e)
        {
            ibdSoftLandForce.ReadValue();     // Read and check the value
        }

        private void EnableInputFields()
        {
            txtLandedDistanceMin.Enabled = chkCheckLandedDistance.Checked;
            txtLandedDistanceMax.Enabled = chkCheckLandedDistance.Checked;
        }
        
        private void chkCheckLandedDistance_CheckedChanged(object sender, EventArgs e)
        {
            EnableInputFields();
        }
             
        private void txtTuningLinearP_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningLinearP_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningLinearP_Leave(object sender, EventArgs e)
        {
            ibdTuningLinearP.ReadValue();     // Read and check the value
        }

        private void txtTuningLinearI_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningLinearI_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningLinearI_Leave(object sender, EventArgs e)
        {
            ibdTuningLinearI.ReadValue();     // Read and check the value
        }

        private void txtTuningLinearD_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningLinearD_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningLinearD_Leave(object sender, EventArgs e)
        {
            ibdTuningLinearD.ReadValue();     // Read and check the value
        }
           
                       
        private void txtSoftlandSpeed_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSoftlandSpeed_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSoftlandSpeed_Leave(object sender, EventArgs e)
        {
            ibdSoftlandSpeed.ReadValue();     // Read and check the value
        }


        private void txtSpeed_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSpeed_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSpeed_Leave(object sender, EventArgs e)
        {
            ibdSpeed.ReadValue();     // Read and check the value
        }



        private void txtSoftlandSensitivity_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSoftlandSensitivity_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSoftlandSensitivity_Leave(object sender, EventArgs e)
        {
            
                ibdSoftlandSensitivity.ReadValue() ;
            
        }

        private void cmbActuator_SelectedIndexChanged(object sender, EventArgs e)
        {
            actuator.Load(cmbActuator.SelectedItem.ToString());
            load_values_from_actuator_ini();

        }

        private void txtDwellTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtDwellTime_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtDwellTime_Leave(object sender, EventArgs e)
        {
            ibdDwellTime.ReadValue();     // Read and check the value
        }

        private void SetButtons(bool bConnected)
        {
            btnSaveToController.Enabled = bConnected;
            btnTeach.Enabled = bConnected;
            btnSingleCycle.Enabled = bConnected;
            btnRun.Enabled = bConnected;
            btnStop.Enabled = bConnected;
            button2.Enabled = bConnected;
        }

        private void NotImplemented()
        {
            MessageBox.Show("Not implemented yet");
        }

        private void LoadSettingsFromFile(string filename)
        {
            MySettings settings = new MySettings();
            settings.Read(filename);
            fileName = filename;
            Title();
            checkerFile.Load();
        }


        //
        // ConfirmChanges() returns true in the following conditions:
        //
        // - There are no changes
        // - There are changes and the operator selects OK in the warning meassage to discard the changes
        // - There are changes and the operator has selected to save the changes and actually has saved them
        //

        private bool ConfirmChanges(string message)
        {
            if (!NoActuatorfilesFound)
            {
                if (checkerFile.Changed())
                {
                    if (MessageBox.Show("You made changes that are not saved" + Environment.NewLine + Environment.NewLine +
                                        "Select OK to " + message + " and discard the changes" + Environment.NewLine +
                                        "Select Cancel to save your changes first", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                                        == DialogResult.OK)
                    {
                        return true;
                    }
                    else
                    {
                        return SaveSettingsToFile();
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }


        //The below code is to load the settings from already saved file 
        private void btnLoadSettingsFromFile_Click(object sender, EventArgs e)
        {
            bool bOk = ConfirmChanges("load new settings from a file");
            if (bOk)
            {
                OpenFileDialog filedialog = new OpenFileDialog();
                filedialog.Filter = "SMAC Leak Test Center files (*.sltc)|*.sltc";
                filedialog.DefaultExt = "sltc";
                filedialog.FileName = Path.GetFileName(fileName);
                if (filedialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = filedialog.FileName;     // Set the new fillename here
                    LoadSettingsFromFile(fileName);
                    Title();
                }
            }
        }


        // The below code is to save the settings to file 
        private bool SaveSettingsToFile()
        {
            SaveFileDialog filedialog = new SaveFileDialog();
            filedialog.FileName = Path.GetFileName(fileName);
            filedialog.Filter = "SMAC Leak Test Center files(*.sltc)|*.sltc";
            filedialog.DefaultExt = "sltc";
            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                fileName = filedialog.FileName;
                MySettings settings = new MySettings();
                settings.Write(fileName);
                checkerFile.Load();
                Title();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btnSaveSettingsToFile_Click(object sender, EventArgs e)
        {
            SaveSettingsToFile();
        }

        // When the button teach is clicked "MS240" string will be sent to the controller

        private void btnTeach_Click(object sender, EventArgs e)
        {
            port.DisableHandler();
            if (port.AbortAndWaitForPrompt())
            {
                if (MessageBox.Show("Clear all objects from the test area", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    btnUndo_Click(null, null);
                    port.WriteString("MS240\r");
                                                     
                    
                }

            }

            port.EnableHandler();
        }

       // The below code sents out "MS24" string to the controller when Single cycle button is clicked        

        private void btnSingleCycle_Click(object sender, EventArgs e)
        {
            port.DisableHandler();
            if (port.AbortAndWaitForPrompt())
            {
                port.WriteString("MS24\r");
            }
            port.EnableHandler();
        }

        // The below code sends out "MS0" when Initialize button is clicked
        private void btnRun_Click(object sender, EventArgs e)
        {
            port.DisableHandler();
            if (port.AbortAndWaitForPrompt())
            {
                port.WriteString("MS0\r");
            }
            port.EnableHandler();
        }

        // When the Stop button is clicked, the below string will be sent to the controller

        private void btnStop_Click(object sender, EventArgs e)
        {
            port.DisableHandler();
            if (port.AbortAndWaitForPrompt())
            {
                port.WriteString("0AB,MF\r");
            }
            port.EnableHandler();
        }


        private void btnContactInfo_Click(object sender, EventArgs e)
        {
            DlgContactInfo contactInfo = new DlgContactInfo();

            contactInfo.ShowDialog();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
//            System.Diagnostics.Process.Start(@"c:\users\herman\Testfile.pdf");
            try
            {
                System.Diagnostics.Process.Start("Help document.pdf");
            }
            catch
            {
                MessageBox.Show("Can not open the help document", "File open error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            NotImplemented();
        }

        // The below code assigns different measurment units based on encoder resolution, which is loaded from the .ini file 
        private int LinearEncoderCounts(double value)
        {
            return (int)(0.5 + value / actuator.LinearResoloution() * 1000.0);
        }

        private double LinearResolution(double linearResolution)
        {
            return (double)(actuator.LinearResoloution());
        }

        private double millimeters(int counts)
        {
            return (double)counts * actuator.LinearResoloution() / 1000.0;
        }

       
        private double micrometers(int counts)
        {
            return (double)counts * actuator.LinearResoloution();
        }

        private void cmbPort_MouseDown(object sender, MouseEventArgs e)
        {
            FillPortnames();
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!NoActuatorfilesFound)
            {
                e.Cancel = !ConfirmChanges("quit the application");
                if (cmbPort.SelectedItem != null)
                {
                    Properties.Settings.Default.Port = cmbPort.SelectedItem.ToString();
                }
                Properties.Settings.Default.File = fileName;
                Properties.Settings.Default.Save();
            }
        }

        private void cmbPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            Opencomport();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog filedialog = new SaveFileDialog();
            filedialog.Filter = "Text files (*.txt)|*.txt";
            filedialog.DefaultExt = "txt";
            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                List<string> Listing = new List<string>();
                LEAKTESTPROGRAM program = new LEAKTESTPROGRAM();
                Variables variables = new Variables(Constants.Defaults());
                Listing.Add(programName);
                Listing.Add("+----------------------------------------------------+");
                Listing.Add("|               The master program                   |");
                Listing.Add("+----------------------------------------------------+");
                for (int i = 0; i < program.CountRaw(); i++)
                {
                    Listing.Add(program.GetRawString(i));
                }
                Listing.Add("+--------------------------------------------------------------------+");
                Listing.Add("|              Variables and their default values                    |");
                Listing.Add("+--------------------------------------------------------------------+");

                for (int i = 0; i < variables.Count(); i++)
                {
                    Listing.Add(variables.GetName(i) + "=" + variables.GetValue(i).ToString());
                }
                File.WriteAllLines(filedialog.FileName, Listing.ToArray());
            }
        }

        private void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            txtTerminal.Focus();
        }

        public bool LoggedIn()
        {
            return ((tabControl1.SelectedIndex == 1) || !passwordRequired);
        }

        private void ShowSettupButtons()
        {
            bool show = (tabControl1.SelectedIndex != 0);
            lblPort.Visible = show;
            cmbPort.Visible = show;
            btnLoadSettingsFromFile.Visible = show;
            btnSaveSettingsToFile.Visible = show;
            btnSaveProgramToFile.Visible = show;
            btnSaveToController.Visible = show;
            btnSaveChart.Visible = show;
            btnSingleCycle.Visible = show;
            chkShowAll.Visible = show;
            button2.Visible = show;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            tabControl1.SelectedIndexChanged -= tabControl1_SelectedIndexChanged;
            if (tabControl1.SelectedIndex == 1 || tabControl1.SelectedIndex == 2)
            {
                int newindex = tabControl1.SelectedIndex;
                if (passwordRequired)
                {
                    tabControl1.SelectedIndex = 0;  // Do not show the Setup tab yet
                    DlgPassword password = new DlgPassword("SMAC5807");
                    switch (password.ShowDialog())
                    {
                        case DialogResult.Cancel:
                            newindex = 0;
                            break;
                        case DialogResult.OK:
                            break;
                        case DialogResult.Yes:
                            passwordRequired = false;
                            break;
                    }
                }
                tabControl1.SelectedIndex = newindex;
            }
            ShowSettupButtons();
            tabControl1.SelectedIndexChanged += tabControl1_SelectedIndexChanged;
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            txtTeachedSqLinear.Text = "";
            txtTeachedSqLinearHome.Text = "";
            txtTeachedLandedDis.Text = "";
        }

        
        

        private void lblPluginfoSetup_Click(object sender, EventArgs e)
        {

        }

        // The below code is to select the specific leak test type from the drop down menu and enable or disable text boxes 
        private void cmbLeakTestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbLeakTestType.SelectedIndex == 0)
            {
                txtParkPosition.Enabled = true;
                txtSoftlandSensitivity2.Visible = false;
                txtSoftlandSensitivity.Visible = true;

                txtSoftlandSpeed.Visible = true;
                lblSoftlandSpeed.Visible = true;
                label40.Visible = true;


                txtPushForce.Enabled = true;
                
            }
            else if (cmbLeakTestType.SelectedIndex == 1)
            {
                txtParkPosition.Enabled = true;
                txtSoftlandSensitivity.Visible = false;
                txtSoftlandSensitivity2.Visible = true;

                txtSoftlandSpeed.Visible = false;
                lblSoftlandSpeed.Visible = false;
                label40.Visible = false;


                txtPushForce.Enabled = true;
            }
            else if (cmbLeakTestType.SelectedIndex == 2)
            {
                txtParkPosition.Enabled = true;
               

                txtPushForce.Enabled = true;
            }
            
        }

        private void cmbController_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbActuator.Items.Clear();

            try
            {
                if (Globals.mainForm.cmbController.Text == "LAC-1")
                {
                    foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators\\LAC1\\LAL300-50-75-2","*.ini*",SearchOption.AllDirectories ))
                    {
                        cmbActuator.Items.Add(Path.GetFileNameWithoutExtension(s).Replace('!', ':'));
                    }
                }
                //
                //else
                //{
                  //  foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators\\LAC25", "*.ini*", SearchOption.AllDirectories))
                    //{
                      //  cmbActuator.Items.Add(Path.GetFileNameWithoutExtension(s).Replace('!', ':'));
                    //}
                //}
                
            }
            catch
            {
                MessageBox.Show("No actuator definitions found." + Environment.NewLine +
                                "Re-install the application and then try again.",
                                "Fatal error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                Application.Exit();
            }

            cmbActuator.SelectedIndex = 0;
        }
          

        
       
        private void txtParkPosition_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtParkPosition_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        
        private void txtParkPosition_Leave(object sender, EventArgs e)
        {
            ibdParkPosition.ReadValue();
        }

       
        private void txtMaxDisplacement_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtMaxDisplacement_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSoftlandPosition_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSoftlandPosition_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSoftlandPosition_Leave(object sender, EventArgs e)
        {
            ibdSoftlandPosition.ReadValue();     // Read and check the value
        }

        private void txtMaxDisplacement_Leave(object sender, EventArgs e)
        {
            ibdNumofThreads.ReadValue();
        }

        private void txtTerminal_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        
        private void Tuning_Click(object sender, EventArgs e)
        {

        }

        private void txtDisplacement_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFinalPosition_TextChanged(object sender, EventArgs e)
        {

        }



        private void pictureBoxSetup_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click_1(object sender, EventArgs e)
        {

        }

        private void lblDisplacement_Click(object sender, EventArgs e)
        {

        }

        private void txtSoftlandPosition_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbController_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void btnUndo1_Click(object sender, EventArgs e)
        {

        }
         
          
        private void btnSaveChart_Click(object sender, EventArgs e)
        {
            SaveFileDialog browseFile = new SaveFileDialog();
            browseFile.Filter = "csv files (*.csv)|*.csv";
            browseFile.DefaultExt = "csv";
            if (saveFile != "")
            {
                browseFile.InitialDirectory = Path.GetDirectoryName(saveFile);
                browseFile.FileName = Path.GetFileName(saveFile);
            }

            browseFile.ShowDialog();
            string chosenFile = browseFile.FileName;
            if (chosenFile != "")
            {
                try
                {
                    saveFile = chosenFile;
                    System.IO.StreamWriter outputfile = new System.IO.StreamWriter(chosenFile);
                    outputfile.WriteLine("Cycle Counts" + "," + "Displacement");
                    foreach (System.Windows.Forms.DataVisualization.Charting.DataPoint d in chart1.Series[0].Points)
                    {
                        outputfile.WriteLine(d.XValue.ToString() + "," + d.YValues[0].ToString());
                    }
                    outputfile.Close();
                }
                catch
                {
                    MessageBox.Show("Error writing file " + chosenFile + Environment.NewLine +
                        "Is the file open in another application ?", "Can not write file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
                  
               
    }
}

