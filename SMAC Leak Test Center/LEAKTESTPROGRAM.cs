﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;

namespace SMAC_Leak_Test_Center
{
    class LEAKTESTPROGRAM
    {
        private List<string> theProgram = new List<string>();
        public Variables variables = null;

        private List<string> translateFrom = new List<string>();
        private List<string> translateInto = new List<string>();

        private string[] masterProgram = new string[]
        {
           ";   I/O ALLOCATION",
           "; ==================================",
           "; OUTPUT 0: READY/RETRACTED/HOME",
           "; OUTPUT 1: PASS",
           "; OUTPUT 2: FAIL",
           "; OUTPUT 3: ERROR/ JAM",
           "; INPUT 0: START TEST CYLE",
           "; INPUT 1: NOT USED",
           "; INPUT 2: NOT USED",
           "; INPUT 3: RESET(In Case of Jam Condition)",
           ";",
           "; Register usage:",
           ";",
           "; 19: This is the register for Softland position in encoder counts",
           "; 20: This is the position of the softland in microns",
           "; 21: This is the value of final position in encoder counts",
           "; 22: This is the final position in microns",
           "; 25: This is the displacement(calculated from soft land pos to final positon)",
           "; 30: This gives the displacement without 'D'",
           "; 31: This is the cycle counter register",
           "; 35: Cycle time",
           "; 100: Soft land force for Teach sequence 2",
           "; 105: Move relative away from container for teach seq2 to pickup SQ (!T)",
           "; 106: Comparison of reg 105 to the home position(Safety check)",
           "; 107: Actual move to position for detecting !T (force near container)",
           ";",
           "0MF",
           "EN",
           "RM",
           "; **CLEAR SCREEN, CONFIGURE I/O CHANNELS******",
           "MD0,BR57600,MG\"!I0\"",
           "MD1,CL0,CL1,CL2,CL3,CF0,CF1,CF2,CF3,UM1,SS2",
           ";",
           "; **LOAD PID'S FOR AXIS 1 (Linear) & AXIS 2 (Rotary)",
           "MD2,0MF,PH0,1PM,1SG[P_LINEAR],SI[I_LINEAR],SD[D_LINEAR],IL[IL_LINEAR],FR[FR_LINEAR],RI[RI_LINEAR],OO[OO_LINEAR],MJ4",
           ";",
           "; **** Setting up Interrupt(input 3)********",
           "; **  This Disables the Linear axis when input 3 is activated.",
           "MD4,AL9,LV3,EV3,MJ6",
           ";",
           "; ***Set Servo phasing for rotary to 0 for right handed thread and to 3 for left handed***",
           "; ***Do homing for the linear and then continue park position.***",
           "; ***(default = 0, After Teach it will be 10mm short of teach point )***",
           "MD6,AL0,AR1,AR2,AR30,AL1,AR31,MC10,PM,SQ10000,MA[PARK_POSITION],GO,WS1,MJ25",
           ";",
           ";**Endless loop that can only be exited by the interrupt routine**",
           "; **The interrupt routine is entered by pressing the reset button**",
           ";",
           "MD8,NO,RP",
           ";",
           "; The interrupt routine",
           ";",
           "MD9,0MF,DV3,MG\"!I3N\",WF3,MG\"!I3F\",UM1,MJ0",
           ";",
           "; Homing routine for linear axis",
           ";",
           "MD10,SV[VF_HOMING_LINEAR],SA[AF_HOMING_LINEAR],SQ[QF_HOMING_LINEAR],VM,MN,DI1,GO,WA250",
           "MD11,RW538,IB[HARDSTOP_THRESHOLD_LINEAR],DI0,JR2,RP,1FI,MJ12",
           "MD12,RL448,IC10,MJ13,NO,RW538,IG[INDEX_LIMIT_LINEAR],MJ14,NO,RP",
           "MD13,AB,WA5,PM,GH,MG\"LINEAR HOME/READY\",RC",
           "MD14,MG\"14-NO INDEX\",MG\"!E1\",MF,CN3,MG\"CLEAR OBSTRUCTION,PUSH RESET INPUT\",MJ8",
           ";",
           ";",
           "; Entry point for Single cycle command from user interface(GUI)",
           ";",
           "MD24,MG\"!I1\",MG\"Single cycle\",MJ28",
           ";",
           "; Start of main loop",
           "; - (MG\"!I0N\" is for Input 0 on)",
           "; - Wait for start input(input0) to be OFF",
           "; - Wait for start input(input0) to be ON(MG\"!O0N\" is for Output 0 on)",
           "; - Then clear all used registers and start the overall timer(MG\"!O0123\" is for Outputs 0,1,2,3 are off)",
           ";",
           "MD25,EV3,1SQ12893,CN0,MG\"!OUT0ON\"",
           "MD26,IN0,JR3,NO,JR-3,IF0,MJ27,NO,JR-3",
           "MD27,MG\"!I1\",MG\"IP 0\",MJ28",
           "MD28,UM1,CF0,CF1,CF2,CF3,MJ30",
           ";",
           ";",
           "; Call on various subroutines",
           ";",
           "MD30,MC32,MC35,MC40,MC50,MC75,MC77,MC80,MC90,RL1830,AR35,MG\"Cycle Time\":35,MJ25",
           ";",
           "; ***** Turn off Outputs, Clear Registers ****",
           "MD32,CF0,CF1,CF2,CF3,AL0,AR1,AR2,AR19,AR20,AR21,AR22,AR25,AR35,AR105,AR106,AR107,WL1830,RC",
           ";",
           ";",
           "; Rapid Move close to container",
           "MD35,1PM,MN,1SQ[Q_MOVE_CLOSE_TO_CONTAINER_LINEAR],MC100,MA[POS_CLOSE_TO_CONTAINER_LINEAR],GO,WS50,RC",
           "MD38,SQ[FM_SOFTLAND_FORCE],QM0,SQ[FM_SOFTLAND_FORCE],MJ39",
           "MD39,RL462,IG[FM_SOFTLAND_SENSITIVITY],NO,MJ42,RL494,IG[MAX_PERMISSABLE_STROKE],NO,JR2,RP,MC70,MJ47",
           ";",
           ";",
           "; Velocity mode- Softland on container, If traveled to far, send fail signal",
           ";",
           "MD40,1VM,MN,MC101,DI0,GO,WA100",
           "MD41,RW538,IG[VM_SOFTLAND_SENSITIVITY],NO,MJ42,RL494,IG[MAX_PERMISSABLE_STROKE],NO,JR2,RP,MC70,MJ47",
           ";",
           "; Capture and Report the Soft Land Position(datum), MG\"!S\" is for the Soft Land Position",
           "; Register 20 Contains the Soft Land Position in microns",
           ";",
           "MD42,AL0,AR1,AR2,RL494,AR19,AM[RESOLUTION],AR20,MG\"!S\":20,RC",
           ";",
           "; Check if the datum is within limits",
           "; (MG\"!SLP\" is for Soft Land Pass)",
           "; (MG\"!SLFH\" Soft Land Failed +)",
           "; (MG\"!SLFL\" Soft Land Failed -)",
           ";",
           "MD45,RA19,IB[LANDED_DISTANCE_MIN],MJ46,NO,IG[LANDED_DISTANCE_MAX],MJ47,NO,MG\"!SLP\",MG\"HEIGHT OK\",RC",
           ";",
           "MD46,1PM,MN,MC100,GH,WS20,CN2,MG\"!SLF+\",MG\"46-PART TOO CLOSE\",MJ6",
           "MD47,1PM,MN,MC100,GH,WS20,CN2,MG\"!SLF-\",MG\"47-PART TOO FAR/NO PART PRESENT\",MJ6",
           ";",
           "; Switch to Force Mode and Push for xxx ms Dwell time",
           ";",
           "MD50,SQ[Q_PUSH_FORCE],QM0,SQ[Q_PUSH_FORCE],WA[DWELL_TIME],MJ51",
           ";",
           "; Record & Report the Final Position(Register 22) in microns",
           "; (MG\"!F\", Final Position)",
           ";",
           "MD51,AL0,AR1,AR2,RL494,AR21,AM[RESOLUTION],AR22,MG\"!F\":22,MJ70",
           ";",
           "; ***** Retract To Home Position ******",
           "MD70,PM,MN,MC100,MA[PARK_POSITION],GO,WS20,RC",
           ";",
           "; ***** Calculate Displacement ********",
           "; REG22 - REG20 = REG25, AM1(or AM other for different encoder resolution)",
           "; **** The Soft land and final values are swapped in the front end to find the difference,..REG25 is the final value of displacement in microns****",
           "MD75,RA22,AS@20,AR25,MG\"!D\":25,AL0,AR1,AR2,RA25,AD5,AR26,RC",
           "MD77,RA25,AR30,MG\"!\":30:N,MG\",\":31,RA31,AA1,AR31,RC",
           ";",
           "; ****** Compare To Limits******",
           "MD80,RA26,IG[MAX_DISP_LIM],NO,MJ82,MG\"!P\",MG\"PASSED\",CN1,RC",
           ";",
           "; **** !FL = Failed Leaking *****",
           "MD82,CN2,MG\"!FL\",MG\"FAILED-LEAKING\",RC",
           ";",
           "; **** Verify that the Actuator is Retracted****",
           "MD90,RL494,IB[PARK_POSITION_2MM],MG\"RETRACTED\",RC,MJ91",
           ";",
           "; **** ! Failure, Linear Not Retracted****",
           "MD91,MF,CN3,MG\"!FLNR\",MG\"Linear failed to retract and motor is turned off\",WN3,WF3,MJ6",
           ";",
           "; ****************************",
           "; ***** Speed Settings ********",
           "MD100,SA[A_HIGH_SPEED],SV[V_HIGH_SPEED],SQ[Q_HIGH_SPEED],RC",
           "MD101,SA[A_SOFTLAND_SPEED_LINEAR],SV[V_SOFTLAND_SPEED_LINEAR],SQ[Q_SOFT_LAND_FORCE_LINEAR],RC",
           ";",
           "; Routine to teach linear force at landing position:",
           ";",
           "; - Do a homing, wait 3 seconds and report linear force(format !Rnnnn)",
           "; - Softland on the part(without initial fast move)",
           "; - Do a position move to 2.0 pitch above softland position",
           "; - Report softland position(format !Lnnnn)",
           "; - After a wait of 1 second report THRO at this position(format !Qnnnn)",
           "; - Go back home",
           ";",
           "MD240,MC10,WA3000,AL0,AR100,AR105,AR106,AR107,RW530,MG\"!R\":0",
           "MD241,1PM,SQ30000,VM,MN,SQ[Q_SETUP_LINEAR],SV[V_SETUP_LINEAR],SA[A_SETUP_LINEAR],DI0,GO,WA100",
           "MD242,RW538,IG2500,MJ243,NO,RP",
           "MD243,MG\"!L\":0,1ST,1PM,MR-5000,GO,WS1000",
           "MD244,RW530,MG\"!Q\":0,AA[SOFTLAND_FORCE_OFFSET_TEACH2],AR100,PM,GH,WS100,SQ10000,EP",
           ";",
           ";",
           "MD246,AL[REG_105_COMPARISON],AR105,WA1000,VM,SQ@100,SV[V_SETUP_LINEAR],SA[A_SETUP_LINEAR],DI0,GO,WA100",
           "MD247,RW538,IG[SOFTLAND_POS_ERR_TEACH2],MJ248,NO,RP",
           "MD248,RL494,MG\"!M\":0,1ST,AS@105,AR106,IG0,AR107,MJ249,AL0,AR107",
           "MD249,PM,MA@107,GO,WS1000,RW530,MG\"!T\":0,MA[PARK_POSITION],GO,WS100,MG\"\",MG\"\",MJ25",
           ";",
          
    };

        private string[] masterProgram26 = new string[]
        {
         ";   I/O ALLOCATION",
           "; ==================================",
           "; OUTPUT 0: READY/RETRACTED/HOME",
           "; OUTPUT 1: PASS",
           "; OUTPUT 2: FAIL",
           "; OUTPUT 3: ERROR/ JAM",
           "; INPUT 0: START TEST CYLE",
           "; INPUT 1: NOT USED",
           "; INPUT 2: NOT USED",
           "; INPUT 3: RESET(In Case of Jam Condition)",
           ";",
           "; Register usage:",
           ";",
           "; 19: This is the register for Softland position in encoder counts",
           "; 20: This is the position of the softland in microns",
           "; 21: This is the value of final position in encoder counts",
           "; 22: This is the final position in microns",
           "; 25: This is the displacement(calculated from soft land pos to final positon)",
           "; 30: This gives the displacement without 'D'",
           "; 31: This is the cycle counter register",
           "; 35: Cycle time",
           "; 100: Soft land force for Teach sequence 2",
           "; 105: Move relative away from container for teach seq2 to pickup SQ (!T)",
           "; 106: Comparison of reg 105 to the home position(Safety check)",
           "; 107: Actual move to position for detecting !T (force near container)",
           ";",
           "0MF",
           "EN",
           "RM",
           "; **CLEAR SCREEN, CONFIGURE I/O CHANNELS******",
           "MD0,BR57600,MG\"!I0\"",
           "MD1,CL0,CL1,CL2,CL3,CF0,CF1,CF2,CF3,UM1,SS2",
           ";",
           "; **LOAD PID'S FOR AXIS 1 (Linear) & AXIS 2 (Rotary)",
           "MD2,0MF,PH0,1PM,1SG[P_LINEAR],SI[I_LINEAR],SD[D_LINEAR],IL[IL_LINEAR],FR[FR_LINEAR],RI[RI_LINEAR],OO[OO_LINEAR],MJ4",
           ";",
           "; **** Setting up Interrupt(input 3)********",
           "; **  This Disables the Linear axis when input 3 is activated.",
           "MD4,AL9,LV3,EV3,MJ6",
           ";",
           "; ***Set Servo phasing for rotary to 0 for right handed thread and to 3 for left handed***",
           "; ***Do homing for the linear and then continue park position.***",
           "; ***(default = 0, After Teach it will be 10mm short of teach point )***",
           "MD6,AL0,AR1,AR2,AR30,AR31,MC10,PM,SQ10000,MA[PARK_POSITION],GO,WS1,MJ25",
           ";",
           ";**Endless loop that can only be exited by the interrupt routine**",
           "; **The interrupt routine is entered by pressing the reset button**",
           ";",
           "MD8,NO,RP",
           ";",
           "; The interrupt routine",
           ";",
           "MD9,0MF,DV3,MG\"!I3N\",WF3,MG\"!I3F\",UM1,MJ0",
           ";",
           "; Homing routine for linear axis",
           ";",
           "MD10,SV[VF_HOMING_LINEAR],SA[AF_HOMING_LINEAR],SQ[QF_HOMING_LINEAR],VM,MN,DI1,GO,WA250",
           "MD11,RW538,IB[HARDSTOP_THRESHOLD_LINEAR],DI0,JR2,RP,1FI,MJ12",
           "MD12,RL448,IC10,MJ13,NO,RW538,IG[INDEX_LIMIT_LINEAR],MJ14,NO,RP",
           "MD13,AB,WA5,PM,GH,MG\"LINEAR HOME/READY\",RC",
           "MD14,MG\"14-NO INDEX\",MG\"!E1\",MF,CN3,MG\"CLEAR OBSTRUCTION,PUSH RESET INPUT\",MJ8",
           ";",
           ";",
           "; Entry point for Single cycle command from user interface(GUI)",
           ";",
           "MD24,MG\"!I1\",MG\"Single cycle\",MJ28",
           ";",
           "; Start of main loop",
           "; - (MG\"!I0N\" is for Input 0 on)",
           "; - Wait for start input(input0) to be OFF",
           "; - Wait for start input(input0) to be ON(MG\"!O0N\" is for Output 0 on)",
           "; - Then clear all used registers and start the overall timer(MG\"!O0123\" is for Outputs 0,1,2,3 are off)",
           ";",
           "MD25,EV3,1SQ12893,CN0,MG\"!OUT0ON\"",
           "MD26,IN0,JR3,NO,JR-3,IF0,MJ27,NO,JR-3",
           "MD27,MG\"!I1\",MG\"IP 0\",MJ28",
           "MD28,UM1,CF0,CF1,CF2,CF3,MJ30",
           ";",
           ";",
           "; Call on various subroutines",
           ";",
           "MD30,",
           ";",
           "; ***** Turn off Outputs, Clear Registers ****",
           "MD32,CF0,CF1,CF2,CF3,AL0,AR1,AR2,AR19,AR20,AR21,AR22,AR25,AR35,WL1830,RC",
           ";",
           ";",
           "; Rapid Move close to container",
           "MD35,1PM,MN,1SQ[Q_MOVE_CLOSE_TO_CONTAINER_LINEAR],MC100,MA[POS_CLOSE_TO_CONTAINER_LINEAR],GO,WS50,RC",
           "MD38,SQ[FM_SOFTLAND_FORCE],QM0,SQ[FM_SOFTLAND_FORCE],MJ41",
           "MD39,RW538,IG[FM_SOFTLAND_SENSITIVITY],NO,MJ42,RL494,IG[MAX_PERMISSABLE_STROKE],NO,JR2,RP,MC70,MJ47",
           ";",
           ";",
           "; Velocity mode- Softland on container, If traveled to far, send fail signal",
           ";",
           "MD40,1VM,MN,MC101,DI0,GO,WA100",
           "MD41,RW538,IG[VM_SOFTLAND_SENSITIVITY],NO,MJ42,RL494,IG[MAX_PERMISSABLE_STROKE],NO,JR2,RP,MC70,MJ47",
           ";",
           "; Capture and Report the Soft Land Position(datum), MG\"!S\" is for the Soft Land Position",
           "; Register 20 Contains the Soft Land Position in microns",
           ";",
           "MD42,AL0,AR1,AR2,RL494,AR19,AM[RESOLUTION],AR20,MG\"!S\":20,RC",
           ";",
           "; Check if the datum is within limits",
           "; (MG\"!SLP\" is for Soft Land Pass)",
           "; (MG\"!SLFH\" Soft Land Failed +)",
           "; (MG\"!SLFL\" Soft Land Failed -)",
           ";",
           "MD45,RA19,IB[LANDED_DISTANCE_MIN],MJ46,NO,IG[LANDED_DISTANCE_MAX],MJ47,NO,MG\"!SLP\",MG\"HEIGHT OK\",RC",
           ";",
           "MD46,1PM,MN,MC100,GH,WS20,CN2,MG\"!SLF+\",MG\"46-PART TOO CLOSE\",MJ6",
           "MD47,1PM,MN,MC100,GH,WS20,CN2,MG\"!SLF-\",MG\"47-PART TOO FAR/NO PART PRESENT\",MJ6",
           ";",
           "; Switch to Force Mode and Push for xxx ms Dwell time",
           ";",
           "MD50,SQ[Q_PUSH_FORCE],QM0,SQ[Q_PUSH_FORCE],WA[DWELL_TIME],MJ51",
           ";",
           "; Record & Report the Final Position(Register 22) in microns",
           "; (MG\"!F\", Final Position)",
           ";",
           "MD51,AL0,AR1,AR2,RL494,AR21,AM[RESOLUTION],AR22,MG\"!F\":22,MJ70",
           ";",
           "; ***** Retract To Home Position ******",
           "MD70,PM,MN,MC100,MA[PARK_POSITION],GO,WS5,RC",
           ";",
           "; ***** Calculate Displacement ********",
           "; REG22 - REG20 = REG25, AM1(or AM other for different encoder resolution)",
           "; **** The Soft land and final values are swapped in the front end to find the difference,..REG25 is the final value of displacement in microns****",
           "MD75,RA22,AS@20,AR25,MG\"!D\":25,AL0,AR1,AR2,RA25,AD5,AR26,RC",
           "MD77,RA25,AR30,MG\"!\":30:N,MG\",\":31,RA31,AA1,AR31,RC",
           ";",
           "; ****** Compare To Limits******",
           "MD80,RA26,IG[MAX_DISP_LIM],NO,MJ82,MG\"!P\",MG\"PASSED\",CN1,RC",
           ";",
           "; **** !FL = Failed Leaking *****",
           "MD82,CN2,MG\"!FL\",MG\"FAILED-LEAKING\",RC",
           ";",
           "; **** Verify that the Actuator is Retracted****",
           "MD90,RL494,IB[PARK_POSITION_2MM],MG\"RETRACTED\",RC,MJ91",
           ";",
           "; **** ! Failure, Linear Not Retracted****",
           "MD91,MF,CN3,MG\"!FLNR\",MG\"Linear failed to retract and motor is turned off\",WN3,WF3,MJ6",
           ";",
           "; ****************************",
           "; ***** Speed Settings ********",
           "MD100,SA[A_HIGH_SPEED],SV[V_HIGH_SPEED],SQ[Q_HIGH_SPEED],RC",
           "MD101,SA[A_SOFTLAND_SPEED_LINEAR],SV[V_SOFTLAND_SPEED_LINEAR],SQ[Q_SOFT_LAND_FORCE_LINEAR],RC",
           ";",
           "; Routine to teach linear force at landing position:",
           ";",
           "; - Do a homing, wait 3 seconds and report linear force(format !Rnnnn)",
           "; - Softland on the part(without initial fast move)",
           "; - Do a position move to 2.0 pitch above softland position",
           "; - Report softland position(format !Lnnnn)",
           "; - After a wait of 1 second report THRO at this position(format !Qnnnn)",
           "; - Go back home",
           ";",
           "MD240,MC10,WA3000,AL0,AR100,AR105,AR106,AR107,RW530,MG\"!R\":0",
           "MD241,1PM,SQ30000,VM,MN,SQ[Q_SETUP_LINEAR],SV[V_SETUP_LINEAR],SA[A_SETUP_LINEAR],DI0,GO,WA100",
           "MD242,RW538,IG2500,MJ243,NO,RP",
           "MD243,MG\"!L\":0,1ST,1PM,MR-5000,GO,WS1000",
           "MD244,RW530,MG\"!Q\":0,AR100,PM,GH,WS100,SQ10000,EP",
           ";",
           ";",
           "MD246,AL[REG_105_COMPARISON],AR105,WA1000,VM,SQ@100,SV[V_SETUP_LINEAR],SA[A_SETUP_LINEAR],DI0,GO,WA100",
           "MD247,RW538,IG[SOFTLAND_POS_ERR_TEACH2],MJ248,NO,RP",
           "MD248,RL494,MG\"!M\":0,1ST,AS@105,AR106,IG0,AR107,MJ249,AL0,AR107",
           "MD249,PM,MA@107,GO,WS1000,RW530,MG\"!T\":0,MA[PARK_POSITION],GO,WS100,MG\"\",MG\"\",MJ25",
           ";",


        };

        // The below code is to count the number of lines in the program

        public int CountRaw()
        {
            if (Globals.mainForm.cmbController.Text == "LAC-26")
            {
                return masterProgram26.Count();
            }
            else
            {
                return masterProgram.Count();
            }
        }

        //
        // Summary: Get untranslated code at a given line in the master program
        //
        public string GetRawString(int index)
        {
            if (Globals.mainForm.cmbController.Text == "LAC-26")
            {
                return masterProgram26[index];
            }
            else
            {
                return masterProgram[index];
            }
        }

        // This was not created by VIGNESH:: This is to subtract 5 from Linear stroke value and move to "MAX_PERMISSABLE_STROKE"
        

        public void SetAHighSpeed()
        {
            variables.GetValue("A_HIGH_SPEED");
            variables.GetValue("A_SOFTLAND_SPEED");  
        }

        public int Count()      // Only valid after a call to Create
        {
            return theProgram.Count;
        }

        public string GetLine(int index) // Only valid after a call to Create
        {
            Trace.Assert(index < theProgram.Count, "Invalid index " + index.ToString());
            return theProgram[index];
        }

        public void Show()
        {
            foreach (string s in masterProgram)
            {
                MessageBox.Show(s);
            }
        }

        // Get global speed of the application:
        // Apply the settin to all accelereations, all speeds and all times
        public void SetSoftlandSpeed(double factor)
        {
            for (int i = 0; i < variables.Count(); i++)
            {
                Variable var = variables.Get(i);
                if ((var.Name.IndexOf("V_SOFTLAND_SPEED_LINEAR") == 0))
                {
                    variables.Multiply(var.Name, factor);
                }
            }
        }

        // The below code is used to set V_HIGH_SPEED from the value form .ini file and user input value from front end
        public void SetSpeed(double factor)
        {
            for (int i = 0; i < variables.Count(); i++)
            {
                Variable var = variables.Get(i);
                if ((var.Name.IndexOf("V_HIGH_SPEED") == 0))
                {
                    variables.Multiply(var.Name, factor);
                }
            }
        }

        // The below value is used to set V_FAST_MOVE parameter from AF_HOMING_LINEAR from .ini file

        public void SetFastMoveParameters( )
        {
            
            variables.SetValue("V_FAST_MOVE", variables.GetValue("AF_HOMING_LINEAR") * 2);
            variables.SetValue("Q_FAST_MOVE", limitLinearSQ((int)variables.GetValue("Q_HOME_LINEAR")+5000));
        }

             
        // The below code gets the max disp value from the front end and loads it into the master program and also alter macro 75 and 243 based on encoder resolution
        public void SetMaxDispLim(int encoderCounts)
        {
            variables.SetValue("MAX_DISP_LIM", encoderCounts);  

            if (variables.GetValue("RESOLUTION") == 1)
            {
                Translate("MD75,", "MD75,RA22,AS@20,AR25,MG\"!D\":25,AL0,AR1,AR2,RA25,AM1,AR26,RC");
                Translate("MD243,", "MD243, RL494, MG\"!L\":0,1ST,1PM,MR-5000,GO,WS1000");
                variables.SetValue("REG_105_COMPARISON", 10000); // MACRO 246 paramater loading based on encoder resolution
                
            }

            if (variables.GetValue("RESOLUTION") == 0.5)
            {
                Translate("MD75,", "MD75,RA22,AS@20,AR25,MG\"!D\":25,AL0,AR1,AR2,RA25,AM2,AR26,RC");
                Translate("MD243,", "MD243, RL494, MG\"!L\":0,1ST,1PM,MR-10000,GO,WS1000");
                variables.SetValue("REG_105_COMPARISON", 20000); // MACRO 246 paramater loading based on encoder resolution
            }

            if (variables.GetValue("RESOLUTION") == 0.1)
            {
                Translate("MD75,", "MD75,RA22,AS@20,AR25,MG\"!D\":25,AL0,AR1,AR2,RA25,AM10,AR26,RC");
                Translate("MD243,", "MD243, RL494, MG\"!L\":0,1ST,1PM,MR-50000,GO,WS1000");
                variables.SetValue("REG_105_COMPARISON", 100000);  // MACRO 246 paramater loading based on encoder resolution
            }

            if (variables.GetValue("RESOLUTION") == 5)
            {
                Translate("MD243,", "MD243, RL494, MG\"!L\":0,1ST,1PM,MR-200,GO,WS1000");
                variables.SetValue("REG_105_COMPARISON", 400); // MACRO 246 paramater loading based on encoder resolution
            }
        }
        

        // The below code sets displacement from the user entered value
        public void SetDisplacement(int encoderCounts)// 12/20: Vignesh
        {
            variables.SetValue("DISPLACEMENT", encoderCounts);
        }
        // The below code sets final position from the user entered value
        public void SetFinalPosition(int encoderCounts)// 12/20: Vignesh
        {
            variables.SetValue("FINAL_POSITION", encoderCounts);
        }

        // The below code sets softland position from the user entered value
        public void SetSoftlandPosition(int encoderCounts)// 12/20: Vignesh
        {
            variables.SetValue("SOFTLAND_POSITION", encoderCounts);
        }
        // The below code sets dwell time from the user entered value
        public void SetDwellTime(int encoderCounts)// 12/19: Vignesh
        {
           
            variables.SetValue("DWELL_TIME", encoderCounts/10);

        }

        
        
        // ADDED ON 01/24:: VIGNESH :: The following code is to get the Park position input from GUI in "mm" using counts
        public void SetParkPosition(double encoderCounts)
        {
            if (variables.GetValue("RESOLUTION") == 1)
            {
                variables.SetValue("PARK_POSITION", (encoderCounts / (variables.GetValue("RESOLUTION"))));
            }

            if(variables.GetValue("RESOLUTION") == 5)
            {
                variables.SetValue("PARK_POSITION", encoderCounts);
                variables.SetValue("PARK_POSITION_2MM", (variables.GetValue("PARK_POSITION") + 80));
            }

            if (variables.GetValue("RESOLUTION") == 0.5)
            {
                variables.SetValue("PARK_POSITION", ((encoderCounts /(variables.GetValue("RESOLUTION" ))))/2);
            }

            if (variables.GetValue("RESOLUTION") == 0.1)
            {
                variables.SetValue("PARK_POSITION", (encoderCounts *10 / 1* (variables.GetValue("RESOLUTION"))));
            }
        }

        //The below code is to add 2 to PArk position in mm and then multiply by 1000( to convert to counts) and then divide by encoder resolution) :: VIGNESH :: 02/01
        public void SetParkPositionAdd(double encoderCounts)
        {
            variables.SetValue("PARK_POSITION_2MM", encoderCounts);
        }

        // added to get the softland sensitivity value from the GUI and use it in the leak test program -VIGNESH:: 01/23::
        public void SetSoftlandSensitivity(double counts)
        {
            variables.SetValue("VM_SOFTLAND_SENSITIVITY", counts);
        }

        public void SetSoftlandSensitivity2(double counts)
        {
            variables.SetValue("FM_SOFTLAND_SENSITIVITY", counts);
        }

        

        public void CheckMovementSelected(double LeakTestType)
        {
            if (LeakTestType == 0)
            {

               // Translate("MD30,", "MD30, MC32, MC35, MC40, MC45, MC50, MC75, MC77, MC80, MC90, RL1830, AR35, MG\"Cycle Time\":35,MG\"RETRACTED\",MJ25");
                Translate("MD38", ";");
                Translate("MD39", ";");

                //[FM_SOFTLAND_FORCE]
                //[FM_SOFTLAND_SENSITIVITY]

            }

            if (LeakTestType ==1)
            {


                Translate("MD40",";");
                Translate("MD41",";");
               // Translate("MD30,", "MD30,MC32,MC35,MC38,MC45,MC50,MC75,MC77,MC80,MC90,RL1830,AR35,MG\"Cycle Time\":35,MG\"RETRACTED\",MJ25");
                //[FM_SOFTLAND_FORCE]
                //[FM_SOFTLAND_SENSITIVITY]

            }
        }

        // The following code sends out Macro 30  and two variables based on different leak test type selected
        public void SetLeakTestType(string LeakTestType)
        {
                     
            switch (LeakTestType)
            {
              
                case Globals.SOFTLAND_AND_PUSH_VEL_MODE:
                    variables.GetValue("VM_SOFTLAND_SENSITIVITY");
                    Translate("MD30,", "MD30,MC32,MC35,MC40,MC45,MC50,MC75,MC77,MC80,MC90,RL1830,AR35,MG\"Cycle Time\":35,MG\"RETRACTED\",MJ25");

                    break;
                case Globals.SOFTLAND_AND_PUSH_FOR_MODE:
                    variables.GetValue("FM_SOFTLAND_SENSITIVITY");
                    variables.GetValue("FM_SOFTLAND_FORCE");
                    Translate("MD30,","MD30,MC32,MC35,MC38,MC45,MC50,MC75,MC77,MC80,MC90,RL1830,AR35,MG\"Cycle Time\":35,MG\"RETRACTED\",MJ25");
                    break;
            }
        }

      
        public void ResetTranslation()
        {
            translateFrom.Clear();
            translateInto.Clear();
        }

        public void Translate(string from, string into)
        {
            translateFrom.Add(from);
            translateInto.Add(into);
        }

        
        // The below code changes the AD value based on different resolution enocder loaded from .ini file

        public void SetResolution(double linearResolution)
        {
            if (linearResolution == 1) 
            {
                variables.SetValue("RESOLUTION", 1);
            }

            if (linearResolution == 5)
            {
                variables.SetValue("RESOLUTION", 5);
                
            }
            
            else if (linearResolution == 0.5)
            {
                variables.SetValue("RESOLUTION", 0.5);  
                Translate ("MD42,", "MD42,AL0,AR1,AR2,RL494,AR19,AD2,AR20,MG\"!S\":20,RC");
                Translate ("MD51,", "MD51,AL0,AR1,AR2,RL494,AR21,AD2,AR22,MG\"!F\":22,MJ70");
            }

            else if (linearResolution == 0.1)
            {
                variables.SetValue("RESOLUTION", 0.1);  
                Translate("MD42,", "MD42,AL0,AR1,AR2,RL494,AR19,AD10,AR20,MG\"!S\":20,RC");
                Translate("MD51,", "MD51,AL0,AR1,AR2,RL494,AR21,AD10,AR22,MG\"!F\":22,MJ70");
            }

        }
               
           
                
        public void EnableCheckLandedDistance(bool enable)
        {
            if(!enable)
            {
                if (Globals.mainForm.cmbLeakTestType.SelectedIndex == 0)
                {
                    Translate("MD30", "MD30,MC32,MC35,MC40,MC50,MC75,MC77,MC80,MC90,RL1830,AR35,MG\"Cycle Time\":35,MG\"RETRACTED\",MJ25");
                }

                //When Check landed distance check box is selected MC45 is added into MD30 string otherwise MD30 will not have MC45  02/07::VIGNESH  
                if(Globals.mainForm.cmbLeakTestType.SelectedIndex == 1)
                {
                    Translate("MD30", "MD30,MC32,MC35,MC38,MC50,MC75,MC77,MC80,MC90,RL1830,AR35,MG\"Cycle Time\":35,MG\"RETRACTED\",MJ25");
                }
                
            }
        }

        private void LimitTorque(string variableName, int maxvalue)
        {
            if (variables.GetValue(variableName) > maxvalue)
            {
                variables.SetValue(variableName, maxvalue);
            }
        }

        

        private int limitLinearSQ(int sq)
        {
            if (sq > 32000)
            {
                sq = 32000;
            }
            return sq;
        }


        public void setLinearForce(string teachedSQ)
        {
            if (teachedSQ != "")
            {
                int sq = int.Parse(teachedSQ);

               
                variables.SetValue("Q_FLOAT_LINEAR", limitLinearSQ(sq + 1000));
                variables.SetValue("Q_RETRACT_LINEAR", limitLinearSQ(sq - 2000));

            }
        }

        public void setLinearForceHome(string teachedSQ)
        {
            if (teachedSQ != "")
            {
                int sq = int.Parse(teachedSQ) + 4000;
                variables.SetValue("Q_HOME_LINEAR", limitLinearSQ(sq + 4000));
            }
        }

        public void SetLinearPID(double P, double I, double D)
        {
            variables.Multiply("P_LINEAR", P / 100.0);
            variables.Multiply("I_LINEAR", I / 100.0);
            variables.Multiply("D_LINEAR", D / 100.0);
        }

        public void SetPushForce(double factor)
        {
            for (int i = 0; i < variables.Count(); i++)
            {
                Variable var = variables.Get(i);
                if ((var.Name.IndexOf("Q_PUSH_FORCE") == 0))
                {
                    variables.Multiply(var.Name, factor);
                }
            }
        }

        public void SetSoftLandForce(double factor)
        {
            for (int i = 0; i < variables.Count(); i++)
            {
                Variable var = variables.Get(i);
                if ((var.Name.IndexOf("Q_SOFT_LAND_FORCE_LINEAR") == 0))
                {
                    variables.Multiply(var.Name, factor);
                }
                if  ((var.Name.IndexOf("FM_SOFTLAND_FORCE") == 0))
                {
                    variables.Multiply(var.Name, factor);
                }
            }
        }
               
        public void SetPositionCloseToContainer(int encoderCounts)
        {
            variables.SetValue("POS_CLOSE_TO_CONTAINER_LINEAR", encoderCounts);
        }

        public void SetMaxPermissableStroke(int linearStroke)  // Added to have the "MAX_PERMISSABLE_STROKE" value to be 5 less than full stroke value, which is taken from ini file 
        {

            variables.SetValue("MAX_PERMISSABLE_STROKE", linearStroke);


          if (variables.GetValue("RESOLUTION") == 5)
          {
                variables.SetValue("MAX_PERMISSABLE_STROKE", (variables.GetValue("MAX_PERMISSABLE_STROKE")) / 2);
          }


          if (variables.GetValue("RESOLUTION") == 0.5)
          {
                variables.SetValue("MAX_PERMISSABLE_STROKE", (variables.GetValue("MAX_PERMISSABLE_STROKE")) / 2);
          }
        }
       

      

        public void SetLandedHeightMin(int encoderCounts)
        {
            variables.SetValue("LANDED_DISTANCE_MIN", encoderCounts);
        }

        public void SetLandedHeightMax(int encoderCounts)
        {
            variables.SetValue("LANDED_DISTANCE_MAX", encoderCounts);
        }

         public void SetSafeMinimumRetractDistance(int encoderCounts)
        {
            variables.SetValue("SAFE_MIN_RETRACT_POS", encoderCounts);
        }

        public void SetSoftlandForceOffsetTeach(int encoderCounts)
        {
            variables.SetValue("SOFTLAND_FORCE_OFFSET_TEACH2", encoderCounts);
        }

        /*public void SetVSoftlandSpeed(int encoderCounts)
        {
            variables.GetValue("V_SOFTLAND_SPEED_LINEAR", encoderCounts);

        }*/

        public void SetSafeParkPosition(int encoderCounts)
        {
            variables.SetValue("SAFE_PARK_POS", encoderCounts);
        }



        public void SetSoftlandPosErrTeach2(int encoderCounts)
        {
            variables.SetValue("SOFTLAND_POS_ERR_TEACH2", encoderCounts);
        }

        //public void SetThreadDepth(int encoderCounts)
        //{
        //    variables.SetValue("1.0_DEPTH_LINEAR", encoderCounts);
        //}

        public void SetPushPullLimit(int encoderCounts)
        {
            variables.SetValue("PUSH_PULL_LIMIT", encoderCounts);
        }

        public bool Create()
        {
            theProgram.Clear();
            if (Globals.mainForm.cmbController.Text == "LAC-26")
            {
                foreach (string s in masterProgram26)
                {
                    string compiled = s.Trim();
                    if (compiled.Length > 0)    // Do not copy empty lines
                    {
                        if (compiled[0] != ';')     // Do not substitute variables in comment lines
                        {
                            for (int i = 0; i < variables.Count(); i++)
                            {
                                Variable v = variables.Get(i);
                                string strTemp = "[" + v.Name + "]";
                                while (compiled.IndexOf(strTemp) > 0)
                                {
                                    compiled = compiled.Replace(strTemp, v.Value.ToString());
                                }
                            }

                            int index1 = compiled.IndexOf('['); // The first delimiter
                            int index2 = compiled.IndexOf(']', index1 + 1);

                            Trace.Assert(index1 == index2, "Unresolved variable in string " + compiled);

                        }

                        for (int i = 0; i < translateFrom.Count; i++)
                        {
                            if (compiled.IndexOf(translateFrom[i]) == 0)
                            {
                                compiled = translateInto[i];
                                break;
                            }
                        }

                        if (compiled.Trim() != "")
                        {
                            theProgram.Add(compiled);
                        }
                    }
                }
            }
            else
            {
                foreach (string s in masterProgram)
                {
                    string compiled = s.Trim();
                    if (compiled.Length > 0)    // Do not copy empty lines
                    {
                        if (compiled[0] != ';')     // Do not substitute variables in comment lines
                        {
                            for (int i = 0; i < variables.Count(); i++)
                            {
                                Variable v = variables.Get(i);
                                string strTemp = "[" + v.Name + "]";
                                while (compiled.IndexOf(strTemp) > 0)
                                {
                                    compiled = compiled.Replace(strTemp, v.Value.ToString());
                                }
                            }

                            int index1 = compiled.IndexOf('['); // The first delimiter
                            int index2 = compiled.IndexOf(']', index1 + 1);

                            Trace.Assert(index1 == index2, "Unresolved variable in string " + compiled);

                        }

                        for (int i = 0; i < translateFrom.Count; i++)
                        {
                            if (compiled.IndexOf(translateFrom[i]) == 0)
                            {
                                compiled = translateInto[i];
                                break;
                            }
                        }

                        if (compiled.Trim() != "")
                        {
                            theProgram.Add(compiled);
                        }
                    }
                }
            }
            return true;
        }

    }
    public static class Constants
    {
        public static string[] Defaults()
        {
            return new string[]
            {
                "P_LINEAR, 12",
                "I_LINEAR, 50",
                "D_LINEAR, 600",
                "IL_LINEAR, 8000",
                "FR_LINEAR, 1",
                "RI_LINEAR, 0",
                "OO_LINEAR, 0",
                "VF_HOMING_LINEAR, 1500000",
                "AF_HOMING_LINEAR, 20000",
                "QF_HOMING_LINEAR, 20000",
                "Q_MOVE_CLOSE_TO_CONTAINER_LINEAR, 25000",
                "Q_HOME_LINEAR, 20000",
                "HARDSTOP_THRESHOLD_LINEAR, -2500",
                "INDEX_LIMIT_LINEAR, 2500",
                "Q_SETUP_LINEAR, 25000",
                "V_SETUP_LINEAR, 500000",
                "A_SETUP_LINEAR, 20000",
                "A_HIGH_SPEED, 80000",// 12/20: Vignesh
                "V_HIGH_SPEED, 200000", // 12/20: Vignesh
                "Q_HIGH_SPEED, 22000", // 12/20: Vignesh
                "A_SOFTLAND_SPEED_LINEAR, 10000",//12/20: Vignesh
                "V_SOFTLAND_SPEED_LINEAR, 20000", //  12/20: Vignesh
                "Q_SOFT_LAND_FORCE_LINEAR, 20000",  //  12/20: Vignesh
                "DWELL_TIME, 300",
                "SOFTLAND_POS_ERR_TEACH2, 250",
                "FM_SOFTLAND_SENSITIVITY, 300",
                "FM_SOFTLAND_FORCE,350000",
                "VM_SOFTLAND_SENSITIVITY, 2500",
                "SOFTLAND_FORCE_OFFSET_TEACH2, 0",
                
                //
                // The following values are filled in by the program
                //
                "POS_CLOSE_TO_CONTAINER_LINEAR, 45000",
                "LANDED_DISTANCE_MIN, 46000",
                "LANDED_DISTANCE_MAX, 50000",
                "SAFE_MIN_RETRACT_POS, 1000",
                "MIN_LINEAR_DISTANCE, 1000",
                "MAX_LINEAR_DISTANCE, 1000",
                "Q_RETRACT_LINEAR, 18000",
                "V_FAST_MOVE, 2000000",
                "A_FAST_MOVE, 2000000",
                "Q_FAST_MOVE, 200000",
                "SAFE_PARK_POS, 1000",
                "MAX_DISP_LIM, 50",
                "DISPLACEMENT,0",
                "SOFTLAND_POSITION, 0",
                "FINAL_POSITION, 0",
                "VM_SOFTLAND_SENSTIVITY_TEST, 0",
                "PARK_POSITION, 25",
                "DEMO, 0",
                "Q_PUSH_FORCE, 32000",
                "Q_SOFT_LAND_FORCE, 0",
                "PARK_POSITION_2MM, 0",
                "MAX_PERMISSABLE_STROKE, 0",
                "RESOLUTION, 0",
                "REG_105_COMPARISON, 0",

            };
        }
    }

}
