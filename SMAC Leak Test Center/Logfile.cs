﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

//
// Logfile will be created in the users "My documents" folder
// Constructor: Logfile(filename)
//
// After setting the Enabled property the first Add() or Addline() call will create the logfile
//
// Properties:
// Enabled (bool)   - true
//                  - false
// Timestamp (bool) - true: a 7 digit timestamp (in msec) will be added to each log item
//                  - false: no time stamp will be added
//

namespace SMAC_Leak_Test_Center
{
    class Logfile
    {
        StreamWriter streamWriter = null;
        private string fileName = "Logfile.txt";

        //
        // The Filename property
        //

        public string Filename
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }
        private bool enabled = false;

        //
        // The Enabled property
        //

        public bool Enabled
        {
            get 
            {
               return enabled; 
            }
            set 
            {
                enabled = value; 
                if (enabled && (streamWriter == null))
                { 
                        try
                        {
                        //                            MessageBox.Show(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + fileName);
                        streamWriter = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + fileName);
                            streamWriter.WriteLine ("Log started " + DateTime.Now.ToString());
                            stopwatch.Reset();
                            stopwatch.Start();
                        }
                        catch
                        {
                        }
                }
                else if (!enabled && (streamWriter != null)) // Not enabled anymore
                {
                    streamWriter.Close();
                    streamWriter = null;
                }
            }
        }

        //
        // The Timestamp property
        //

        private bool timestamp = false;
        public bool Timestamp
        {
            get
            {
                return timestamp;
            }
            set
            {
                timestamp = value;
            }
        }

        private bool first = true;

        private Stopwatch stopwatch = new Stopwatch();

        public Logfile()
        {
        }

        public Logfile(string filename)     // The constructor
        {
            fileName = filename;
        }

        ~Logfile()      // the destructor
        {
 //           if (streamWriter != null)
 //           {
 //               streamWriter.Close();
 //           }

        }
        public void Add(string s)
        {
            if (enabled)
            {
                try
                {
                    if (first && timestamp)
                    {
                        streamWriter.Write(stopwatch.ElapsedMilliseconds.ToString("D8") + " ");
                        first = false;
                    }
                    streamWriter.Write(s);
                    streamWriter.Flush();
                }
                catch
                {
                }
            }
        }

        public void AddLine(string s)
        {
            Add(s);
            Add(Environment.NewLine);
            first = true;
        }
    }
}
