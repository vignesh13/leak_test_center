﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Ports;
using System.Windows.Forms;
using System.Diagnostics;

namespace SMAC_Leak_Test_Center
{
    public class Serial
    {
        private int oldBaud = 9600;     // This is the baudrate on entering 9600 baud mode

        private bool silentmode = false;
        private SerialPort port = new SerialPort();
        private SerialDataReceivedEventHandler theHandler;
        private string buffer = "";
        private Logfile logFile = new Logfile();

        private static string prompt = "\r\n>";


        public Serial()     // The constructor
        {
            logFile.Filename = "Serial port logging.txt";
            logFile.Timestamp = true;
            logFile.Enabled = true;         // Wil start the logfile
        }


        //Connects to Serial Port based on Baud Rate and Port Name.  All other settings are static:
        //No handshaking, no parity, one stop bit, 500ms write timeout, 250ms read timeout, 8 data bits

        public void Open(string strPortname, int iBaudrate, SerialDataReceivedEventHandler handler)
        {
            try
            {
                logFile.Timestamp = true;
                logFile.Enabled = true;
                port.PortName = strPortname;
                port.ReceivedBytesThreshold = 1;
                port.BaudRate = iBaudrate;
                port.DataBits = 8;
                port.WriteTimeout = 500;
                port.ReadTimeout = 500;
                port.Handshake = System.IO.Ports.Handshake.None;
                port.Parity = Parity.None;
                port.StopBits = StopBits.One;
                port.NewLine = "\r";            // Must be forced to be a single CR character
                //                MessageBox.Show(port.ReadBufferSize.ToString());
                if (!port.IsOpen)
                {
                    Globals.mainForm.lblStatus.Text = "Connecting ...";
                    Globals.mainForm.lblStatus.BackColor = System.Drawing.Color.Yellow;
                    Globals.mainForm.lblStatus.ForeColor = System.Drawing.Color.Black;

                    Globals.mainForm.Refresh();
                    

                    port.Open();
                    Thread.Sleep(100);  // Some delay
                    port.ReadExisting();
                    if (!SetBaudrate(iBaudrate))
                    {
                        MessageBox.Show
                            ("No controller found on " + strPortname + Environment.NewLine +
                             "Please check the following:" + Environment.NewLine +
                             "- The serial cable is connected." + Environment.NewLine +
                             "- The controller is powered." + Environment.NewLine,
                            "No controller",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        port.Close();
                    }
                    else
                    {
                        theHandler = new SerialDataReceivedEventHandler(handler);
                        port.DataReceived += theHandler;       // Add the received handler
//                        AbortAndWaitForPrompt();
                        buffer = "";
                    }
                }
                else
                {
                    MessageBox.Show("Port Already Opened");
                }
            }
            catch
            {
                MessageBox.Show(strPortname + " is already opened by another program." + Environment.NewLine + Environment.NewLine +
                    "Please close the program that uses this port and then try again.",
                    "Can not open serial port", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Globals.mainForm.lblStatus.ForeColor = System.Drawing.Color.White;
            if (port.IsOpen)
            {
                Globals.mainForm.lblStatus.Text = "Connected";
                Globals.mainForm.lblStatus.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                Globals.mainForm.lblStatus.Text = "Not connected";
                Globals.mainForm.lblStatus.BackColor = System.Drawing.Color.Red;
            }
        }


        public string GetAllBytes()
        {
            string returnValue = "";
            if (port.BytesToRead > 0)
            {
                returnValue = buffer + port.ReadExisting();
            }
            buffer = "";
            return returnValue;
        }

        public bool SetBaudrate(int baudrate)
        {
            int[] baudrates = new int[4] { 9600, 19200, 38400, 57600};
            bool oldSilentmode = silentmode;
            silentmode = true;
            for (int i = 0; i < baudrates.Count(); i++)
            {
                port.BaudRate = baudrates[i];
                Thread.Sleep(100);
                if (AbortAndWaitForPrompt())
                {
                    Thread.Sleep(100);
                    WriteString("BR" + baudrate.ToString() + "\r");
                    Thread.Sleep(100);
                    port.BaudRate = baudrate;
                    Thread.Sleep(100);
                    port.ReadExisting();
                    silentmode = oldSilentmode;
                    return true;
                }
            }
            silentmode = oldSilentmode;
            return false;
        }

        public void Mode9600(bool on)
        {
            // store old baudrate, log new one, set and test 9600
            if (on)
            {
                oldBaud = port.BaudRate;
                WriteString("BR9600\r");
                Thread.Sleep(100);
                port.BaudRate = 9600;
                port.ReadExisting();
            }
            // log and test old baudrate
            else
            {

                WriteString("BR" + oldBaud.ToString() + "\r");
                Thread.Sleep(2000);
                port.BaudRate = oldBaud;
                port.ReadExisting();
                Thread.Sleep(100);         //Summary1: 10/17/2017 by Vignesh: Added to include a MS0 at the end of "Save program to controller" event //
                port.Write("\u001b,MS0\r"); // The baudrate gets changed from 9600 to 57600 during this time and timer is added to give MS0 to load and function within the time //
                Thread.Sleep(200);

            }

        }

        public void WriteString(string strStringToWrite)
        {
            if (port.IsOpen)
            {
                logFile.Add(">" + strStringToWrite + "\n");
                port.Write(strStringToWrite);
            }
        }

        public void DisableHandler()
        {
            port.DataReceived -= theHandler;
        }

        public void EnableHandler()
        {
            port.DataReceived += theHandler;
        }

        public bool WaitForPrompt()
        {
            return WaitForPrompt(500);
        }

        public bool WaitForPrompt(int msec)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            while (stopwatch.ElapsedMilliseconds < msec)
            {
                if (port.BytesToRead > 0)
                {
                    Globals.mainForm.txtTerminal.AppendText(port.ReadExisting());
                    int lastindex = Globals.mainForm.txtTerminal.Text.Length - 1;
                    if ((lastindex >= 2) && (Globals.mainForm.txtTerminal.Text.Substring(lastindex - 2) == prompt))
                    {
                        return true;
                    }
                }
            }
            if (!silentmode)
            {
                MessageBox.Show("No response from the controller" + Environment.NewLine + Environment.NewLine +
                    "Make sure that:" + Environment.NewLine + Environment.NewLine +
                    "- The right COM port is selected." + Environment.NewLine +
                    "- The serial cable is connected to the controller." + Environment.NewLine +
                    "- The controller is powered.",
                    "Comminication error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            return false;
        }


        public bool WriteStringAndWaitForPrompt(string s)
        {
            if (port.IsOpen)
            {
                port.Write(s);
                return WaitForPrompt();
            }
            else
            {
                return false;
            }
        }


        public bool AbortAndWaitForPrompt()
        {
            return WriteStringAndWaitForPrompt("\u001b");       // Write an escape character
        }


        public bool IsOpen()
        {
            return port.IsOpen;
        }


        public void Close()
        {
            if (port.IsOpen)
            {

                port.DataReceived -= theHandler;    // Get rid of the handler
                theHandler = null;
                port.Close();
                Thread.Sleep(100);  // Wait some time
            }
        }
    }
}
