﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace SMAC_Leak_Test_Center
{
    public class Variable
    {
        private String strName = "";
        private double intVal = 0;

        public Variable(string name, double value) // The constructor
        {
            strName = name;
            intVal = value;
        }


        public string Name
        {
            get
            {
                return strName;
            }
            set
            {
                strName = value;
            }
        }

        public double Value
        {
            get
            {
                return intVal;
            }
            set
            {
                intVal = value;
            }
        }


    }

    public class Variables
    {
        public List<Variable> variables = new List<Variable>();

        public Variables(string[] inistrings)       // The constructor
        {
            Load(inistrings);
        }

        public Variables()                          // Constructor
        {
            variables.Clear();
        }

        public void Add(Variable var)
        {
            variables.Add(var);
        }

        public int Count()
        {
            return variables.Count;
        }

        public Variable Get(int index)
        {
            return variables[index];
        }

        private int Indexof(string name)
        {
            for (int i = 0; i < variables.Count; i++)
            {
                if (variables[i].Name == name)
                {
                    return i;
                }
            }
            Trace.Assert(true, "No such variable : " + name);
            return -1;
        }

        public void SetValue(string name, double value)
        {
            int index = Indexof(name);
            if (index >= 0)
            {
                variables[index].Value = value;
            }
            
        }

        public void InvertDirection(string name)
        {
            switch (GetValue(name))
            {
                case 0:
                    SetValue(name, 1);
                    break;
                default:
                    SetValue(name, 0);
                    break;
            }
        }

        public void Invert(string name)
        {
            SetValue(name, -1 * GetValue(name));
        }

        public void Multiply(string name, double factor)
        {
            SetValue(name, (int)(0.5 + (double)(GetValue(name) * factor)));    // The 0.5 is to assure proper rounding
        }

        public double GetValue(int index)
        {
            return (variables[index].Value);
        }

        public String GetName(int index)
        {
            return (variables[index].Name);
        }



        public double GetValue(string name)
        {
            int index = Indexof(name);
            if (index >= 0)
            {
                return variables[index].Value;
            }
            return 0;           // Must return something
        }

        public bool Load(string[] iniStrings)      // The constructor. Each line is in the form %00%,Value
        {
            variables.Clear();
            foreach (string s in iniStrings)
            {
                string[] fields = s.Split(',');
                for (int i = 0; i < fields.Count(); i++)
                {
                    fields[i] = fields[i].Trim();
                }
                Trace.Assert(fields.Count() == 2, "Incorrect fieldcount in string " + s);
                Trace.Assert(fields[0].Length >= 0, "No name field in string " + s);
                Trace.Assert(fields[1].Length >= 0, "No value field in string " + s);
                int intVal = 0;
                Trace.Assert(int.TryParse(fields[1], out intVal), "No valid value in string " + s);
                variables.Add(new Variable(fields[0], intVal));
            }
            return true;
        }
    }
}
