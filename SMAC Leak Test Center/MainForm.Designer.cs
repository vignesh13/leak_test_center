﻿namespace SMAC_Leak_Test_Center
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.txtPositionCloseToContainer = new System.Windows.Forms.TextBox();
            this.lblPositionCloseToPart = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbActuator = new System.Windows.Forms.ComboBox();
            this.lblActuator = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.cmbPort = new System.Windows.Forms.ComboBox();
            this.btnLoadSettingsFromFile = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label15 = new System.Windows.Forms.Label();
            this.btnActuatorInfo = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Run = new System.Windows.Forms.TabPage();
            this.lblDisplacementRun = new System.Windows.Forms.Label();
            this.lblDisplacementRun1 = new System.Windows.Forms.Label();
            this.pictureBoxRun = new System.Windows.Forms.PictureBox();
            this.Setup = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDisplacementSetup1 = new System.Windows.Forms.Label();
            this.lblDisplacementSetup = new System.Windows.Forms.Label();
            this.txtSoftlandPosition = new System.Windows.Forms.TextBox();
            this.lblSoftlandPosition = new System.Windows.Forms.Label();
            this.txtDisplacement = new System.Windows.Forms.TextBox();
            this.lblDisplacement = new System.Windows.Forms.Label();
            this.txtFinalPosition = new System.Windows.Forms.TextBox();
            this.lblFinalPosition = new System.Windows.Forms.Label();
            this.lblController = new System.Windows.Forms.Label();
            this.cmbController = new System.Windows.Forms.ComboBox();
            this.txtMaxDisplacement = new System.Windows.Forms.TextBox();
            this.lblMaxDispLim = new System.Windows.Forms.Label();
            this.txtParkPosition = new System.Windows.Forms.TextBox();
            this.lblParkPosition = new System.Windows.Forms.Label();
            this.cmbLeakTestType = new System.Windows.Forms.ComboBox();
            this.lblLeakTestType = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.btnTeach = new System.Windows.Forms.Button();
            this.lblLandedDistanceMax = new System.Windows.Forms.Label();
            this.lblLandedDistanceMin = new System.Windows.Forms.Label();
            this.txtLandedDistanceMax = new System.Windows.Forms.TextBox();
            this.txtLandedDistanceMin = new System.Windows.Forms.TextBox();
            this.chkCheckLandedDistance = new System.Windows.Forms.CheckBox();
            this.pictureBoxSetup = new System.Windows.Forms.PictureBox();
            this.Tuning = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSpeed = new System.Windows.Forms.TextBox();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.txtSoftlandSensitivity2 = new System.Windows.Forms.TextBox();
            this.gbxTeachResult1 = new System.Windows.Forms.GroupBox();
            this.btnUndo1 = new System.Windows.Forms.Button();
            this.txtTeached2LandedDis = new System.Windows.Forms.TextBox();
            this.txtTeached2SqLinear = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDwellTime = new System.Windows.Forms.TextBox();
            this.lblDwellTime = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPushForce = new System.Windows.Forms.TextBox();
            this.lblPushForce = new System.Windows.Forms.Label();
            this.gbxTuningRotary = new System.Windows.Forms.GroupBox();
            this.gbxTuningLinear = new System.Windows.Forms.GroupBox();
            this.txtTuningLinearP = new System.Windows.Forms.TextBox();
            this.txtTuningLinearD = new System.Windows.Forms.TextBox();
            this.lblTuningLinearP = new System.Windows.Forms.Label();
            this.txtTuningLinearI = new System.Windows.Forms.TextBox();
            this.lblTuningLinearI = new System.Windows.Forms.Label();
            this.lblTuningLinearD = new System.Windows.Forms.Label();
            this.lblTuningLinearDUnits = new System.Windows.Forms.Label();
            this.lblTuningLinearPUnits = new System.Windows.Forms.Label();
            this.lblTuningLinearIUnits = new System.Windows.Forms.Label();
            this.gbxTeachResult = new System.Windows.Forms.GroupBox();
            this.btnUndo = new System.Windows.Forms.Button();
            this.txtTeachedLandedDis = new System.Windows.Forms.TextBox();
            this.txtTeachedSqLinearHome = new System.Windows.Forms.TextBox();
            this.txtTeachedSqLinear = new System.Windows.Forms.TextBox();
            this.lblTeachedLandedDis = new System.Windows.Forms.Label();
            this.lblTeachedSqLinearHome = new System.Windows.Forms.Label();
            this.lblTeachedSqLinear = new System.Windows.Forms.Label();
            this.lblTeachedHeightUnits = new System.Windows.Forms.Label();
            this.lblSoftlandSensitivity = new System.Windows.Forms.Label();
            this.lblSoftlandSpeed = new System.Windows.Forms.Label();
            this.txtSoftlandSensitivity = new System.Windows.Forms.TextBox();
            this.txtSoftlandSpeed = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSoftLandForce = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.lblSoftLandForce = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.lblLinearDistance_Setup = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnSaveProgramToFile = new System.Windows.Forms.Button();
            this.btnSingleCycle = new System.Windows.Forms.Button();
            this.btnSaveToController = new System.Windows.Forms.Button();
            this.btnSaveSettingsToFile = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnContactInfo = new System.Windows.Forms.Button();
            this.btnHelpDocument = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.chkShowAll = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblStatusTxt = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnSaveChart = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.Run.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRun)).BeginInit();
            this.Setup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSetup)).BeginInit();
            this.Tuning.SuspendLayout();
            this.gbxTeachResult1.SuspendLayout();
            this.gbxTuningLinear.SuspendLayout();
            this.gbxTeachResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTerminal
            // 
            this.txtTerminal.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTerminal.Location = new System.Drawing.Point(541, 514);
            this.txtTerminal.Margin = new System.Windows.Forms.Padding(4);
            this.txtTerminal.Multiline = true;
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtTerminal.Size = new System.Drawing.Size(556, 334);
            this.txtTerminal.TabIndex = 0;
            this.txtTerminal.WordWrap = false;
            this.txtTerminal.TextChanged += new System.EventHandler(this.txtTerminal_TextChanged);
            this.txtTerminal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTerminal_KeyPress);
            // 
            // txtPositionCloseToContainer
            // 
            this.txtPositionCloseToContainer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPositionCloseToContainer.Location = new System.Drawing.Point(864, 137);
            this.txtPositionCloseToContainer.Margin = new System.Windows.Forms.Padding(4);
            this.txtPositionCloseToContainer.Name = "txtPositionCloseToContainer";
            this.txtPositionCloseToContainer.Size = new System.Drawing.Size(72, 22);
            this.txtPositionCloseToContainer.TabIndex = 504;
            this.txtPositionCloseToContainer.Text = "50";
            this.txtPositionCloseToContainer.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPositionCloseToContainer_KeyUp);
            this.txtPositionCloseToContainer.Leave += new System.EventHandler(this.txtPositionCloseToContainer_Leave);
            // 
            // lblPositionCloseToPart
            // 
            this.lblPositionCloseToPart.AutoSize = true;
            this.lblPositionCloseToPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPositionCloseToPart.Location = new System.Drawing.Point(626, 138);
            this.lblPositionCloseToPart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPositionCloseToPart.Name = "lblPositionCloseToPart";
            this.lblPositionCloseToPart.Size = new System.Drawing.Size(217, 17);
            this.lblPositionCloseToPart.TabIndex = 2;
            this.lblPositionCloseToPart.Text = "Position Close To Container [mm]";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(720, 492);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Controller Output";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(643, 338);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 17);
            this.label11.TabIndex = 2;
            // 
            // cmbActuator
            // 
            this.cmbActuator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbActuator.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbActuator.FormattingEnabled = true;
            this.cmbActuator.Location = new System.Drawing.Point(752, 51);
            this.cmbActuator.Margin = new System.Windows.Forms.Padding(4);
            this.cmbActuator.Name = "cmbActuator";
            this.cmbActuator.Size = new System.Drawing.Size(206, 24);
            this.cmbActuator.TabIndex = 500;
            this.cmbActuator.SelectedIndexChanged += new System.EventHandler(this.cmbActuator_SelectedIndexChanged);
            // 
            // lblActuator
            // 
            this.lblActuator.AutoSize = true;
            this.lblActuator.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActuator.Location = new System.Drawing.Point(657, 54);
            this.lblActuator.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblActuator.Name = "lblActuator";
            this.lblActuator.Size = new System.Drawing.Size(61, 17);
            this.lblActuator.TabIndex = 2;
            this.lblActuator.Text = "Actuator";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(1133, 58);
            this.lblPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(34, 17);
            this.lblPort.TabIndex = 2;
            this.lblPort.Text = "Port";
            // 
            // cmbPort
            // 
            this.cmbPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPort.FormattingEnabled = true;
            this.cmbPort.Location = new System.Drawing.Point(1183, 54);
            this.cmbPort.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPort.Name = "cmbPort";
            this.cmbPort.Size = new System.Drawing.Size(104, 24);
            this.cmbPort.Sorted = true;
            this.cmbPort.TabIndex = 109;
            this.cmbPort.SelectedIndexChanged += new System.EventHandler(this.cmbPort_SelectedIndexChanged);
            this.cmbPort.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cmbPort_MouseDown);
            // 
            // btnLoadSettingsFromFile
            // 
            this.btnLoadSettingsFromFile.Location = new System.Drawing.Point(1132, 123);
            this.btnLoadSettingsFromFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoadSettingsFromFile.Name = "btnLoadSettingsFromFile";
            this.btnLoadSettingsFromFile.Size = new System.Drawing.Size(157, 43);
            this.btnLoadSettingsFromFile.TabIndex = 110;
            this.btnLoadSettingsFromFile.Text = "Load Settings From File";
            this.btnLoadSettingsFromFile.UseVisualStyleBackColor = true;
            this.btnLoadSettingsFromFile.Click += new System.EventHandler(this.btnLoadSettingsFromFile_Click);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(0, 514);
            this.chart1.Margin = new System.Windows.Forms.Padding(4);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(516, 335);
            this.chart1.TabIndex = 112;
            this.chart1.Text = "chart1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Location = new System.Drawing.Point(11, 492);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(233, 19);
            this.label15.TabIndex = 113;
            this.label15.Text = "Displacement (mm) vs Cycle counts";
            // 
            // btnActuatorInfo
            // 
            this.btnActuatorInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActuatorInfo.Location = new System.Drawing.Point(969, 23);
            this.btnActuatorInfo.Margin = new System.Windows.Forms.Padding(4);
            this.btnActuatorInfo.Name = "btnActuatorInfo";
            this.btnActuatorInfo.Size = new System.Drawing.Size(65, 23);
            this.btnActuatorInfo.TabIndex = 114;
            this.btnActuatorInfo.Text = "Info";
            this.btnActuatorInfo.UseVisualStyleBackColor = true;
            this.btnActuatorInfo.Click += new System.EventHandler(this.btnActuatorinfo_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Run);
            this.tabControl1.Controls.Add(this.Setup);
            this.tabControl1.Controls.Add(this.Tuning);
            this.tabControl1.Location = new System.Drawing.Point(-4, -1);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1129, 489);
            this.tabControl1.TabIndex = 115;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // Run
            // 
            this.Run.Controls.Add(this.lblDisplacementRun);
            this.Run.Controls.Add(this.lblDisplacementRun1);
            this.Run.Controls.Add(this.pictureBoxRun);
            this.Run.Location = new System.Drawing.Point(4, 25);
            this.Run.Margin = new System.Windows.Forms.Padding(4);
            this.Run.Name = "Run";
            this.Run.Padding = new System.Windows.Forms.Padding(4);
            this.Run.Size = new System.Drawing.Size(1121, 460);
            this.Run.TabIndex = 0;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            // 
            // lblDisplacementRun
            // 
            this.lblDisplacementRun.BackColor = System.Drawing.Color.Transparent;
            this.lblDisplacementRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplacementRun.Location = new System.Drawing.Point(-15, 167);
            this.lblDisplacementRun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDisplacementRun.Name = "lblDisplacementRun";
            this.lblDisplacementRun.Size = new System.Drawing.Size(233, 53);
            this.lblDisplacementRun.TabIndex = 532;
            this.lblDisplacementRun.Text = "Displacement";
            this.lblDisplacementRun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDisplacementRun1
            // 
            this.lblDisplacementRun1.AutoSize = true;
            this.lblDisplacementRun1.BackColor = System.Drawing.Color.Transparent;
            this.lblDisplacementRun1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplacementRun1.ForeColor = System.Drawing.Color.Black;
            this.lblDisplacementRun1.Location = new System.Drawing.Point(23, 236);
            this.lblDisplacementRun1.Name = "lblDisplacementRun1";
            this.lblDisplacementRun1.Size = new System.Drawing.Size(0, 39);
            this.lblDisplacementRun1.TabIndex = 535;
            // 
            // pictureBoxRun
            // 
            this.pictureBoxRun.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pictureBoxRun.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxRun.Image = global::SMAC_Leak_Test_Center.Properties.Resources.leak_test_e;
            this.pictureBoxRun.Location = new System.Drawing.Point(226, 0);
            this.pictureBoxRun.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxRun.Name = "pictureBoxRun";
            this.pictureBoxRun.Size = new System.Drawing.Size(753, 452);
            this.pictureBoxRun.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxRun.TabIndex = 520;
            this.pictureBoxRun.TabStop = false;
            // 
            // Setup
            // 
            this.Setup.Controls.Add(this.label7);
            this.Setup.Controls.Add(this.label10);
            this.Setup.Controls.Add(this.label9);
            this.Setup.Controls.Add(this.label6);
            this.Setup.Controls.Add(this.label5);
            this.Setup.Controls.Add(this.label4);
            this.Setup.Controls.Add(this.label2);
            this.Setup.Controls.Add(this.lblDisplacementSetup1);
            this.Setup.Controls.Add(this.lblDisplacementSetup);
            this.Setup.Controls.Add(this.txtSoftlandPosition);
            this.Setup.Controls.Add(this.lblSoftlandPosition);
            this.Setup.Controls.Add(this.txtDisplacement);
            this.Setup.Controls.Add(this.lblDisplacement);
            this.Setup.Controls.Add(this.txtFinalPosition);
            this.Setup.Controls.Add(this.lblFinalPosition);
            this.Setup.Controls.Add(this.lblController);
            this.Setup.Controls.Add(this.cmbController);
            this.Setup.Controls.Add(this.txtMaxDisplacement);
            this.Setup.Controls.Add(this.lblMaxDispLim);
            this.Setup.Controls.Add(this.txtParkPosition);
            this.Setup.Controls.Add(this.lblParkPosition);
            this.Setup.Controls.Add(this.cmbLeakTestType);
            this.Setup.Controls.Add(this.lblLeakTestType);
            this.Setup.Controls.Add(this.label44);
            this.Setup.Controls.Add(this.label42);
            this.Setup.Controls.Add(this.btnTeach);
            this.Setup.Controls.Add(this.lblLandedDistanceMax);
            this.Setup.Controls.Add(this.lblLandedDistanceMin);
            this.Setup.Controls.Add(this.txtLandedDistanceMax);
            this.Setup.Controls.Add(this.txtLandedDistanceMin);
            this.Setup.Controls.Add(this.chkCheckLandedDistance);
            this.Setup.Controls.Add(this.txtPositionCloseToContainer);
            this.Setup.Controls.Add(this.btnActuatorInfo);
            this.Setup.Controls.Add(this.lblPositionCloseToPart);
            this.Setup.Controls.Add(this.cmbActuator);
            this.Setup.Controls.Add(this.lblActuator);
            this.Setup.Controls.Add(this.pictureBoxSetup);
            this.Setup.Location = new System.Drawing.Point(4, 25);
            this.Setup.Margin = new System.Windows.Forms.Padding(4);
            this.Setup.Name = "Setup";
            this.Setup.Padding = new System.Windows.Forms.Padding(4);
            this.Setup.Size = new System.Drawing.Size(1121, 460);
            this.Setup.TabIndex = 1;
            this.Setup.Text = "Setup";
            this.Setup.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(939, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 17);
            this.label7.TabIndex = 592;
            this.label7.Text = "mm.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(939, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 17);
            this.label10.TabIndex = 591;
            this.label10.Text = "mm.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(939, 209);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 17);
            this.label9.TabIndex = 590;
            this.label9.Text = "mm.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(940, 318);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 17);
            this.label6.TabIndex = 588;
            this.label6.Text = "mm.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(940, 291);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 17);
            this.label5.TabIndex = 587;
            this.label5.Text = "mm.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(940, 264);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 17);
            this.label4.TabIndex = 586;
            this.label4.Text = "mm.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(939, 235);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 585;
            this.label2.Text = "mm.";
            // 
            // lblDisplacementSetup1
            // 
            this.lblDisplacementSetup1.AutoSize = true;
            this.lblDisplacementSetup1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplacementSetup1.ForeColor = System.Drawing.Color.Black;
            this.lblDisplacementSetup1.Location = new System.Drawing.Point(18, 350);
            this.lblDisplacementSetup1.Name = "lblDisplacementSetup1";
            this.lblDisplacementSetup1.Size = new System.Drawing.Size(0, 39);
            this.lblDisplacementSetup1.TabIndex = 584;
            // 
            // lblDisplacementSetup
            // 
            this.lblDisplacementSetup.AutoSize = true;
            this.lblDisplacementSetup.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.lblDisplacementSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplacementSetup.Location = new System.Drawing.Point(22, 276);
            this.lblDisplacementSetup.Name = "lblDisplacementSetup";
            this.lblDisplacementSetup.Size = new System.Drawing.Size(205, 36);
            this.lblDisplacementSetup.TabIndex = 583;
            this.lblDisplacementSetup.Text = "Displacement";
            // 
            // txtSoftlandPosition
            // 
            this.txtSoftlandPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoftlandPosition.Location = new System.Drawing.Point(864, 230);
            this.txtSoftlandPosition.Name = "txtSoftlandPosition";
            this.txtSoftlandPosition.ReadOnly = true;
            this.txtSoftlandPosition.Size = new System.Drawing.Size(72, 22);
            this.txtSoftlandPosition.TabIndex = 582;
            this.txtSoftlandPosition.Text = "10";
            this.txtSoftlandPosition.TextChanged += new System.EventHandler(this.txtSoftlandPosition_TextChanged);
            this.txtSoftlandPosition.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSoftlandPosition_KeyUp);
            this.txtSoftlandPosition.Leave += new System.EventHandler(this.txtSoftlandPosition_Leave);
            // 
            // lblSoftlandPosition
            // 
            this.lblSoftlandPosition.AutoSize = true;
            this.lblSoftlandPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftlandPosition.Location = new System.Drawing.Point(627, 233);
            this.lblSoftlandPosition.Name = "lblSoftlandPosition";
            this.lblSoftlandPosition.Size = new System.Drawing.Size(148, 17);
            this.lblSoftlandPosition.TabIndex = 581;
            this.lblSoftlandPosition.Text = "Softland Position [mm]";
            this.lblSoftlandPosition.Click += new System.EventHandler(this.label4_Click_1);
            // 
            // txtDisplacement
            // 
            this.txtDisplacement.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplacement.Location = new System.Drawing.Point(864, 285);
            this.txtDisplacement.Name = "txtDisplacement";
            this.txtDisplacement.ReadOnly = true;
            this.txtDisplacement.Size = new System.Drawing.Size(72, 22);
            this.txtDisplacement.TabIndex = 577;
            this.txtDisplacement.TextChanged += new System.EventHandler(this.txtDisplacement_TextChanged);
            // 
            // lblDisplacement
            // 
            this.lblDisplacement.AutoSize = true;
            this.lblDisplacement.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplacement.Location = new System.Drawing.Point(626, 285);
            this.lblDisplacement.Name = "lblDisplacement";
            this.lblDisplacement.Size = new System.Drawing.Size(127, 17);
            this.lblDisplacement.TabIndex = 576;
            this.lblDisplacement.Text = "Displacement [mm]";
            this.lblDisplacement.Click += new System.EventHandler(this.lblDisplacement_Click);
            // 
            // txtFinalPosition
            // 
            this.txtFinalPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinalPosition.Location = new System.Drawing.Point(864, 258);
            this.txtFinalPosition.Name = "txtFinalPosition";
            this.txtFinalPosition.ReadOnly = true;
            this.txtFinalPosition.Size = new System.Drawing.Size(72, 22);
            this.txtFinalPosition.TabIndex = 575;
            this.txtFinalPosition.Text = "15";
            this.txtFinalPosition.TextChanged += new System.EventHandler(this.txtFinalPosition_TextChanged);
            // 
            // lblFinalPosition
            // 
            this.lblFinalPosition.AutoSize = true;
            this.lblFinalPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinalPosition.Location = new System.Drawing.Point(627, 257);
            this.lblFinalPosition.Name = "lblFinalPosition";
            this.lblFinalPosition.Size = new System.Drawing.Size(126, 17);
            this.lblFinalPosition.TabIndex = 574;
            this.lblFinalPosition.Text = "Final Position [mm]";
            this.lblFinalPosition.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblController
            // 
            this.lblController.AutoSize = true;
            this.lblController.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblController.Location = new System.Drawing.Point(624, 29);
            this.lblController.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblController.Name = "lblController";
            this.lblController.Size = new System.Drawing.Size(105, 17);
            this.lblController.TabIndex = 573;
            this.lblController.Text = "Controller Type";
            // 
            // cmbController
            // 
            this.cmbController.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbController.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbController.FormattingEnabled = true;
            this.cmbController.Location = new System.Drawing.Point(752, 23);
            this.cmbController.Margin = new System.Windows.Forms.Padding(4);
            this.cmbController.Name = "cmbController";
            this.cmbController.Size = new System.Drawing.Size(206, 24);
            this.cmbController.TabIndex = 572;
            this.cmbController.SelectedIndexChanged += new System.EventHandler(this.cmbController_SelectedIndexChanged_1);
            // 
            // txtMaxDisplacement
            // 
            this.txtMaxDisplacement.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaxDisplacement.Location = new System.Drawing.Point(864, 313);
            this.txtMaxDisplacement.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaxDisplacement.Name = "txtMaxDisplacement";
            this.txtMaxDisplacement.Size = new System.Drawing.Size(72, 22);
            this.txtMaxDisplacement.TabIndex = 568;
            this.txtMaxDisplacement.Text = "1";
            this.txtMaxDisplacement.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxDisplacement_KeyUp);
            this.txtMaxDisplacement.Leave += new System.EventHandler(this.txtMaxDisplacement_Leave);
            // 
            // lblMaxDispLim
            // 
            this.lblMaxDispLim.AutoSize = true;
            this.lblMaxDispLim.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxDispLim.Location = new System.Drawing.Point(627, 315);
            this.lblMaxDispLim.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaxDispLim.Name = "lblMaxDispLim";
            this.lblMaxDispLim.Size = new System.Drawing.Size(189, 17);
            this.lblMaxDispLim.TabIndex = 567;
            this.lblMaxDispLim.Text = "Max Displacement Limit [mm]";
            this.lblMaxDispLim.UseWaitCursor = true;
            // 
            // txtParkPosition
            // 
            this.txtParkPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParkPosition.Location = new System.Drawing.Point(864, 109);
            this.txtParkPosition.Name = "txtParkPosition";
            this.txtParkPosition.Size = new System.Drawing.Size(72, 22);
            this.txtParkPosition.TabIndex = 562;
            this.txtParkPosition.Text = "100";
            this.txtParkPosition.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtParkPosition_KeyUp);
            this.txtParkPosition.Leave += new System.EventHandler(this.txtParkPosition_Leave);
            // 
            // lblParkPosition
            // 
            this.lblParkPosition.AutoSize = true;
            this.lblParkPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParkPosition.Location = new System.Drawing.Point(626, 110);
            this.lblParkPosition.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParkPosition.Name = "lblParkPosition";
            this.lblParkPosition.Size = new System.Drawing.Size(125, 17);
            this.lblParkPosition.TabIndex = 560;
            this.lblParkPosition.Text = "Park Position [mm]";
            // 
            // cmbLeakTestType
            // 
            this.cmbLeakTestType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLeakTestType.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLeakTestType.FormattingEnabled = true;
            this.cmbLeakTestType.Items.AddRange(new object[] {
            "Softland and Push(Velocity Mode)",
            "Softland and Push(Force Mode)"});
            this.cmbLeakTestType.Location = new System.Drawing.Point(752, 79);
            this.cmbLeakTestType.Margin = new System.Windows.Forms.Padding(4);
            this.cmbLeakTestType.Name = "cmbLeakTestType";
            this.cmbLeakTestType.Size = new System.Drawing.Size(206, 24);
            this.cmbLeakTestType.TabIndex = 555;
            this.cmbLeakTestType.SelectedIndexChanged += new System.EventHandler(this.cmbLeakTestType_SelectedIndexChanged);
            // 
            // lblLeakTestType
            // 
            this.lblLeakTestType.AutoSize = true;
            this.lblLeakTestType.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeakTestType.Location = new System.Drawing.Point(624, 79);
            this.lblLeakTestType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLeakTestType.Name = "lblLeakTestType";
            this.lblLeakTestType.Size = new System.Drawing.Size(107, 17);
            this.lblLeakTestType.TabIndex = 554;
            this.lblLeakTestType.Text = "Leak Test Type";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(721, 517);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(88, 17);
            this.label44.TabIndex = 519;
            this.label44.Text = "Teached SQ";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(721, 505);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(88, 17);
            this.label42.TabIndex = 519;
            this.label42.Text = "Teached SQ";
            // 
            // btnTeach
            // 
            this.btnTeach.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTeach.Location = new System.Drawing.Point(945, 135);
            this.btnTeach.Margin = new System.Windows.Forms.Padding(4);
            this.btnTeach.Name = "btnTeach";
            this.btnTeach.Size = new System.Drawing.Size(65, 28);
            this.btnTeach.TabIndex = 120;
            this.btnTeach.Text = "Teach";
            this.btnTeach.UseVisualStyleBackColor = true;
            this.btnTeach.Click += new System.EventHandler(this.btnTeach_Click);
            // 
            // lblLandedDistanceMax
            // 
            this.lblLandedDistanceMax.AutoSize = true;
            this.lblLandedDistanceMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLandedDistanceMax.Location = new System.Drawing.Point(654, 204);
            this.lblLandedDistanceMax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLandedDistanceMax.Name = "lblLandedDistanceMax";
            this.lblLandedDistanceMax.Size = new System.Drawing.Size(178, 17);
            this.lblLandedDistanceMax.TabIndex = 119;
            this.lblLandedDistanceMax.Text = "Landed Distance Max [mm]";
            // 
            // lblLandedDistanceMin
            // 
            this.lblLandedDistanceMin.AutoSize = true;
            this.lblLandedDistanceMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLandedDistanceMin.Location = new System.Drawing.Point(654, 182);
            this.lblLandedDistanceMin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLandedDistanceMin.Name = "lblLandedDistanceMin";
            this.lblLandedDistanceMin.Size = new System.Drawing.Size(175, 17);
            this.lblLandedDistanceMin.TabIndex = 119;
            this.lblLandedDistanceMin.Text = "Landed Distance Min [mm]";
            // 
            // txtLandedDistanceMax
            // 
            this.txtLandedDistanceMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLandedDistanceMax.Location = new System.Drawing.Point(864, 199);
            this.txtLandedDistanceMax.Margin = new System.Windows.Forms.Padding(4);
            this.txtLandedDistanceMax.Name = "txtLandedDistanceMax";
            this.txtLandedDistanceMax.Size = new System.Drawing.Size(72, 22);
            this.txtLandedDistanceMax.TabIndex = 506;
            this.txtLandedDistanceMax.Text = "55";
            this.txtLandedDistanceMax.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPartLocationMax_KeyUp);
            this.txtLandedDistanceMax.Leave += new System.EventHandler(this.txtPartLocationMax_Leave);
            // 
            // txtLandedDistanceMin
            // 
            this.txtLandedDistanceMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLandedDistanceMin.Location = new System.Drawing.Point(864, 174);
            this.txtLandedDistanceMin.Margin = new System.Windows.Forms.Padding(4);
            this.txtLandedDistanceMin.Name = "txtLandedDistanceMin";
            this.txtLandedDistanceMin.Size = new System.Drawing.Size(72, 22);
            this.txtLandedDistanceMin.TabIndex = 505;
            this.txtLandedDistanceMin.Text = "51";
            this.txtLandedDistanceMin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPartLocationMin_KeyUp);
            this.txtLandedDistanceMin.Leave += new System.EventHandler(this.txtPartLocationMin_Leave);
            // 
            // chkCheckLandedDistance
            // 
            this.chkCheckLandedDistance.AutoSize = true;
            this.chkCheckLandedDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCheckLandedDistance.Location = new System.Drawing.Point(630, 160);
            this.chkCheckLandedDistance.Margin = new System.Windows.Forms.Padding(4);
            this.chkCheckLandedDistance.Name = "chkCheckLandedDistance";
            this.chkCheckLandedDistance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCheckLandedDistance.Size = new System.Drawing.Size(180, 21);
            this.chkCheckLandedDistance.TabIndex = 117;
            this.chkCheckLandedDistance.Text = "Check Landed Distance";
            this.chkCheckLandedDistance.UseVisualStyleBackColor = true;
            this.chkCheckLandedDistance.CheckedChanged += new System.EventHandler(this.chkCheckLandedDistance_CheckedChanged);
            // 
            // pictureBoxSetup
            // 
            this.pictureBoxSetup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSetup.Image = global::SMAC_Leak_Test_Center.Properties.Resources.leak_test_side;
            this.pictureBoxSetup.Location = new System.Drawing.Point(0, 4);
            this.pictureBoxSetup.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxSetup.Name = "pictureBoxSetup";
            this.pictureBoxSetup.Size = new System.Drawing.Size(516, 448);
            this.pictureBoxSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSetup.TabIndex = 115;
            this.pictureBoxSetup.TabStop = false;
            this.pictureBoxSetup.Click += new System.EventHandler(this.pictureBoxSetup_Click);
            // 
            // Tuning
            // 
            this.Tuning.Controls.Add(this.label13);
            this.Tuning.Controls.Add(this.txtSpeed);
            this.Tuning.Controls.Add(this.lblSpeed);
            this.Tuning.Controls.Add(this.txtSoftlandSensitivity2);
            this.Tuning.Controls.Add(this.gbxTeachResult1);
            this.Tuning.Controls.Add(this.label1);
            this.Tuning.Controls.Add(this.txtDwellTime);
            this.Tuning.Controls.Add(this.lblDwellTime);
            this.Tuning.Controls.Add(this.label3);
            this.Tuning.Controls.Add(this.txtPushForce);
            this.Tuning.Controls.Add(this.lblPushForce);
            this.Tuning.Controls.Add(this.gbxTuningRotary);
            this.Tuning.Controls.Add(this.gbxTuningLinear);
            this.Tuning.Controls.Add(this.gbxTeachResult);
            this.Tuning.Controls.Add(this.lblSoftlandSensitivity);
            this.Tuning.Controls.Add(this.lblSoftlandSpeed);
            this.Tuning.Controls.Add(this.txtSoftlandSensitivity);
            this.Tuning.Controls.Add(this.txtSoftlandSpeed);
            this.Tuning.Controls.Add(this.label40);
            this.Tuning.Controls.Add(this.label20);
            this.Tuning.Controls.Add(this.label11);
            this.Tuning.Controls.Add(this.txtSoftLandForce);
            this.Tuning.Controls.Add(this.label25);
            this.Tuning.Controls.Add(this.lblSoftLandForce);
            this.Tuning.Location = new System.Drawing.Point(4, 25);
            this.Tuning.Margin = new System.Windows.Forms.Padding(4);
            this.Tuning.Name = "Tuning";
            this.Tuning.Size = new System.Drawing.Size(1121, 460);
            this.Tuning.TabIndex = 2;
            this.Tuning.Text = "Tuning";
            this.Tuning.UseVisualStyleBackColor = true;
            this.Tuning.Click += new System.EventHandler(this.Tuning_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(606, 30);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 17);
            this.label13.TabIndex = 580;
            this.label13.Text = "%";
            // 
            // txtSpeed
            // 
            this.txtSpeed.Location = new System.Drawing.Point(527, 28);
            this.txtSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.txtSpeed.Name = "txtSpeed";
            this.txtSpeed.Size = new System.Drawing.Size(72, 22);
            this.txtSpeed.TabIndex = 579;
            this.txtSpeed.Text = "50";
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(360, 33);
            this.lblSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(49, 17);
            this.lblSpeed.TabIndex = 578;
            this.lblSpeed.Text = "Speed";
            // 
            // txtSoftlandSensitivity2
            // 
            this.txtSoftlandSensitivity2.Location = new System.Drawing.Point(527, 95);
            this.txtSoftlandSensitivity2.Name = "txtSoftlandSensitivity2";
            this.txtSoftlandSensitivity2.Size = new System.Drawing.Size(72, 22);
            this.txtSoftlandSensitivity2.TabIndex = 0;
            // 
            // gbxTeachResult1
            // 
            this.gbxTeachResult1.Controls.Add(this.btnUndo1);
            this.gbxTeachResult1.Controls.Add(this.txtTeached2LandedDis);
            this.gbxTeachResult1.Controls.Add(this.txtTeached2SqLinear);
            this.gbxTeachResult1.Controls.Add(this.label12);
            this.gbxTeachResult1.Controls.Add(this.label14);
            this.gbxTeachResult1.Controls.Add(this.label16);
            this.gbxTeachResult1.Location = new System.Drawing.Point(691, 254);
            this.gbxTeachResult1.Margin = new System.Windows.Forms.Padding(4);
            this.gbxTeachResult1.Name = "gbxTeachResult1";
            this.gbxTeachResult1.Padding = new System.Windows.Forms.Padding(4);
            this.gbxTeachResult1.Size = new System.Drawing.Size(348, 156);
            this.gbxTeachResult1.TabIndex = 577;
            this.gbxTeachResult1.TabStop = false;
            this.gbxTeachResult1.Text = " Teach Result-With Container In Place";
            // 
            // btnUndo1
            // 
            this.btnUndo1.Location = new System.Drawing.Point(224, 59);
            this.btnUndo1.Margin = new System.Windows.Forms.Padding(4);
            this.btnUndo1.Name = "btnUndo1";
            this.btnUndo1.Size = new System.Drawing.Size(65, 28);
            this.btnUndo1.TabIndex = 521;
            this.btnUndo1.Text = "Undo";
            this.btnUndo1.UseVisualStyleBackColor = true;
            this.btnUndo1.Visible = false;
            this.btnUndo1.Click += new System.EventHandler(this.btnUndo1_Click);
            // 
            // txtTeached2LandedDis
            // 
            this.txtTeached2LandedDis.Location = new System.Drawing.Point(142, 103);
            this.txtTeached2LandedDis.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeached2LandedDis.Name = "txtTeached2LandedDis";
            this.txtTeached2LandedDis.ReadOnly = true;
            this.txtTeached2LandedDis.Size = new System.Drawing.Size(67, 22);
            this.txtTeached2LandedDis.TabIndex = 520;
            // 
            // txtTeached2SqLinear
            // 
            this.txtTeached2SqLinear.Location = new System.Drawing.Point(142, 61);
            this.txtTeached2SqLinear.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeached2SqLinear.Name = "txtTeached2SqLinear";
            this.txtTeached2SqLinear.ReadOnly = true;
            this.txtTeached2SqLinear.Size = new System.Drawing.Size(67, 22);
            this.txtTeached2SqLinear.TabIndex = 520;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 106);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 17);
            this.label12.TabIndex = 519;
            this.label12.Text = "Landed Distance";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 65);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 17);
            this.label14.TabIndex = 519;
            this.label14.Text = "SQ Near Container";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(218, 108);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 17);
            this.label16.TabIndex = 2;
            this.label16.Text = "mm.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(611, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 17);
            this.label1.TabIndex = 576;
            this.label1.Text = "ms";
            // 
            // txtDwellTime
            // 
            this.txtDwellTime.Location = new System.Drawing.Point(527, 194);
            this.txtDwellTime.Margin = new System.Windows.Forms.Padding(4);
            this.txtDwellTime.Name = "txtDwellTime";
            this.txtDwellTime.Size = new System.Drawing.Size(72, 22);
            this.txtDwellTime.TabIndex = 575;
            this.txtDwellTime.Text = "150";
            this.txtDwellTime.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDwellTime_KeyUp);
            this.txtDwellTime.Leave += new System.EventHandler(this.txtDwellTime_Leave);
            // 
            // lblDwellTime
            // 
            this.lblDwellTime.AutoSize = true;
            this.lblDwellTime.Location = new System.Drawing.Point(359, 194);
            this.lblDwellTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDwellTime.Name = "lblDwellTime";
            this.lblDwellTime.Size = new System.Drawing.Size(152, 17);
            this.lblDwellTime.TabIndex = 574;
            this.lblDwellTime.Text = "Push Force Dwell Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(609, 165);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 17);
            this.label3.TabIndex = 572;
            this.label3.Text = "%";
            // 
            // txtPushForce
            // 
            this.txtPushForce.Location = new System.Drawing.Point(527, 162);
            this.txtPushForce.Margin = new System.Windows.Forms.Padding(4);
            this.txtPushForce.Name = "txtPushForce";
            this.txtPushForce.Size = new System.Drawing.Size(72, 22);
            this.txtPushForce.TabIndex = 569;
            this.txtPushForce.Text = "100";
            this.txtPushForce.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPushForce_KeyUp);
            this.txtPushForce.Leave += new System.EventHandler(this.txtPushForce_Leave);
            // 
            // lblPushForce
            // 
            this.lblPushForce.AutoSize = true;
            this.lblPushForce.Location = new System.Drawing.Point(359, 165);
            this.lblPushForce.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPushForce.Name = "lblPushForce";
            this.lblPushForce.Size = new System.Drawing.Size(80, 17);
            this.lblPushForce.TabIndex = 568;
            this.lblPushForce.Text = "Push Force";
            // 
            // gbxTuningRotary
            // 
            this.gbxTuningRotary.Location = new System.Drawing.Point(35, 242);
            this.gbxTuningRotary.Margin = new System.Windows.Forms.Padding(4);
            this.gbxTuningRotary.Name = "gbxTuningRotary";
            this.gbxTuningRotary.Padding = new System.Windows.Forms.Padding(4);
            this.gbxTuningRotary.Size = new System.Drawing.Size(173, 167);
            this.gbxTuningRotary.TabIndex = 531;
            this.gbxTuningRotary.TabStop = false;
            this.gbxTuningRotary.Text = " ";
            // 
            // gbxTuningLinear
            // 
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearP);
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearD);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearP);
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearI);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearI);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearD);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearDUnits);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearPUnits);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearIUnits);
            this.gbxTuningLinear.Location = new System.Drawing.Point(35, 47);
            this.gbxTuningLinear.Margin = new System.Windows.Forms.Padding(4);
            this.gbxTuningLinear.Name = "gbxTuningLinear";
            this.gbxTuningLinear.Padding = new System.Windows.Forms.Padding(4);
            this.gbxTuningLinear.Size = new System.Drawing.Size(173, 167);
            this.gbxTuningLinear.TabIndex = 530;
            this.gbxTuningLinear.TabStop = false;
            this.gbxTuningLinear.Text = " Tuning Linear ";
            // 
            // txtTuningLinearP
            // 
            this.txtTuningLinearP.Location = new System.Drawing.Point(48, 38);
            this.txtTuningLinearP.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningLinearP.Name = "txtTuningLinearP";
            this.txtTuningLinearP.Size = new System.Drawing.Size(72, 22);
            this.txtTuningLinearP.TabIndex = 511;
            this.txtTuningLinearP.Text = "100";
            this.txtTuningLinearP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearP_KeyUp);
            this.txtTuningLinearP.Leave += new System.EventHandler(this.txtTuningLinearP_Leave);
            // 
            // txtTuningLinearD
            // 
            this.txtTuningLinearD.Location = new System.Drawing.Point(48, 97);
            this.txtTuningLinearD.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningLinearD.Name = "txtTuningLinearD";
            this.txtTuningLinearD.Size = new System.Drawing.Size(72, 22);
            this.txtTuningLinearD.TabIndex = 513;
            this.txtTuningLinearD.Text = "100";
            this.txtTuningLinearD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearD_KeyUp);
            this.txtTuningLinearD.Leave += new System.EventHandler(this.txtTuningLinearD_Leave);
            // 
            // lblTuningLinearP
            // 
            this.lblTuningLinearP.AutoSize = true;
            this.lblTuningLinearP.Location = new System.Drawing.Point(24, 41);
            this.lblTuningLinearP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearP.Name = "lblTuningLinearP";
            this.lblTuningLinearP.Size = new System.Drawing.Size(17, 17);
            this.lblTuningLinearP.TabIndex = 2;
            this.lblTuningLinearP.Text = "P";
            // 
            // txtTuningLinearI
            // 
            this.txtTuningLinearI.Location = new System.Drawing.Point(48, 68);
            this.txtTuningLinearI.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningLinearI.Name = "txtTuningLinearI";
            this.txtTuningLinearI.Size = new System.Drawing.Size(72, 22);
            this.txtTuningLinearI.TabIndex = 512;
            this.txtTuningLinearI.Text = "100";
            this.txtTuningLinearI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearI_KeyUp);
            this.txtTuningLinearI.Leave += new System.EventHandler(this.txtTuningLinearI_Leave);
            // 
            // lblTuningLinearI
            // 
            this.lblTuningLinearI.AutoSize = true;
            this.lblTuningLinearI.Location = new System.Drawing.Point(24, 70);
            this.lblTuningLinearI.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearI.Name = "lblTuningLinearI";
            this.lblTuningLinearI.Size = new System.Drawing.Size(11, 17);
            this.lblTuningLinearI.TabIndex = 2;
            this.lblTuningLinearI.Text = "I";
            // 
            // lblTuningLinearD
            // 
            this.lblTuningLinearD.AutoSize = true;
            this.lblTuningLinearD.Location = new System.Drawing.Point(24, 100);
            this.lblTuningLinearD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearD.Name = "lblTuningLinearD";
            this.lblTuningLinearD.Size = new System.Drawing.Size(18, 17);
            this.lblTuningLinearD.TabIndex = 2;
            this.lblTuningLinearD.Text = "D";
            // 
            // lblTuningLinearDUnits
            // 
            this.lblTuningLinearDUnits.AutoSize = true;
            this.lblTuningLinearDUnits.Location = new System.Drawing.Point(127, 100);
            this.lblTuningLinearDUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearDUnits.Name = "lblTuningLinearDUnits";
            this.lblTuningLinearDUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningLinearDUnits.TabIndex = 2;
            this.lblTuningLinearDUnits.Text = "%";
            // 
            // lblTuningLinearPUnits
            // 
            this.lblTuningLinearPUnits.AutoSize = true;
            this.lblTuningLinearPUnits.Location = new System.Drawing.Point(127, 41);
            this.lblTuningLinearPUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearPUnits.Name = "lblTuningLinearPUnits";
            this.lblTuningLinearPUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningLinearPUnits.TabIndex = 2;
            this.lblTuningLinearPUnits.Text = "%";
            // 
            // lblTuningLinearIUnits
            // 
            this.lblTuningLinearIUnits.AutoSize = true;
            this.lblTuningLinearIUnits.Location = new System.Drawing.Point(127, 70);
            this.lblTuningLinearIUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearIUnits.Name = "lblTuningLinearIUnits";
            this.lblTuningLinearIUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningLinearIUnits.TabIndex = 2;
            this.lblTuningLinearIUnits.Text = "%";
            // 
            // gbxTeachResult
            // 
            this.gbxTeachResult.Controls.Add(this.btnUndo);
            this.gbxTeachResult.Controls.Add(this.txtTeachedLandedDis);
            this.gbxTeachResult.Controls.Add(this.txtTeachedSqLinearHome);
            this.gbxTeachResult.Controls.Add(this.txtTeachedSqLinear);
            this.gbxTeachResult.Controls.Add(this.lblTeachedLandedDis);
            this.gbxTeachResult.Controls.Add(this.lblTeachedSqLinearHome);
            this.gbxTeachResult.Controls.Add(this.lblTeachedSqLinear);
            this.gbxTeachResult.Controls.Add(this.lblTeachedHeightUnits);
            this.gbxTeachResult.Location = new System.Drawing.Point(335, 254);
            this.gbxTeachResult.Margin = new System.Windows.Forms.Padding(4);
            this.gbxTeachResult.Name = "gbxTeachResult";
            this.gbxTeachResult.Padding = new System.Windows.Forms.Padding(4);
            this.gbxTeachResult.Size = new System.Drawing.Size(348, 156);
            this.gbxTeachResult.TabIndex = 529;
            this.gbxTeachResult.TabStop = false;
            this.gbxTeachResult.Text = " Teach Result-With No Container In Place";
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(252, 60);
            this.btnUndo.Margin = new System.Windows.Forms.Padding(4);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(65, 28);
            this.btnUndo.TabIndex = 521;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Visible = false;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // txtTeachedLandedDis
            // 
            this.txtTeachedLandedDis.Location = new System.Drawing.Point(169, 103);
            this.txtTeachedLandedDis.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeachedLandedDis.Name = "txtTeachedLandedDis";
            this.txtTeachedLandedDis.ReadOnly = true;
            this.txtTeachedLandedDis.Size = new System.Drawing.Size(67, 22);
            this.txtTeachedLandedDis.TabIndex = 520;
            // 
            // txtTeachedSqLinearHome
            // 
            this.txtTeachedSqLinearHome.Location = new System.Drawing.Point(169, 21);
            this.txtTeachedSqLinearHome.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeachedSqLinearHome.Name = "txtTeachedSqLinearHome";
            this.txtTeachedSqLinearHome.ReadOnly = true;
            this.txtTeachedSqLinearHome.Size = new System.Drawing.Size(67, 22);
            this.txtTeachedSqLinearHome.TabIndex = 520;
            // 
            // txtTeachedSqLinear
            // 
            this.txtTeachedSqLinear.Location = new System.Drawing.Point(169, 61);
            this.txtTeachedSqLinear.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeachedSqLinear.Name = "txtTeachedSqLinear";
            this.txtTeachedSqLinear.ReadOnly = true;
            this.txtTeachedSqLinear.Size = new System.Drawing.Size(67, 22);
            this.txtTeachedSqLinear.TabIndex = 520;
            // 
            // lblTeachedLandedDis
            // 
            this.lblTeachedLandedDis.AutoSize = true;
            this.lblTeachedLandedDis.Location = new System.Drawing.Point(5, 105);
            this.lblTeachedLandedDis.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeachedLandedDis.Name = "lblTeachedLandedDis";
            this.lblTeachedLandedDis.Size = new System.Drawing.Size(121, 17);
            this.lblTeachedLandedDis.TabIndex = 519;
            this.lblTeachedLandedDis.Text = "Extended Position";
            // 
            // lblTeachedSqLinearHome
            // 
            this.lblTeachedSqLinearHome.AutoSize = true;
            this.lblTeachedSqLinearHome.Location = new System.Drawing.Point(5, 25);
            this.lblTeachedSqLinearHome.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeachedSqLinearHome.Name = "lblTeachedSqLinearHome";
            this.lblTeachedSqLinearHome.Size = new System.Drawing.Size(94, 17);
            this.lblTeachedSqLinearHome.TabIndex = 519;
            this.lblTeachedSqLinearHome.Text = "SQ Retracted";
            // 
            // lblTeachedSqLinear
            // 
            this.lblTeachedSqLinear.AutoSize = true;
            this.lblTeachedSqLinear.Location = new System.Drawing.Point(4, 65);
            this.lblTeachedSqLinear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeachedSqLinear.Name = "lblTeachedSqLinear";
            this.lblTeachedSqLinear.Size = new System.Drawing.Size(162, 17);
            this.lblTeachedSqLinear.TabIndex = 519;
            this.lblTeachedSqLinear.Text = "SQ At Extended Position";
            // 
            // lblTeachedHeightUnits
            // 
            this.lblTeachedHeightUnits.AutoSize = true;
            this.lblTeachedHeightUnits.Location = new System.Drawing.Point(249, 107);
            this.lblTeachedHeightUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeachedHeightUnits.Name = "lblTeachedHeightUnits";
            this.lblTeachedHeightUnits.Size = new System.Drawing.Size(34, 17);
            this.lblTeachedHeightUnits.TabIndex = 2;
            this.lblTeachedHeightUnits.Text = "mm.";
            // 
            // lblSoftlandSensitivity
            // 
            this.lblSoftlandSensitivity.AutoSize = true;
            this.lblSoftlandSensitivity.Location = new System.Drawing.Point(359, 101);
            this.lblSoftlandSensitivity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSoftlandSensitivity.Name = "lblSoftlandSensitivity";
            this.lblSoftlandSensitivity.Size = new System.Drawing.Size(127, 17);
            this.lblSoftlandSensitivity.TabIndex = 527;
            this.lblSoftlandSensitivity.Text = "Softland Sensitivity";
            // 
            // lblSoftlandSpeed
            // 
            this.lblSoftlandSpeed.AutoSize = true;
            this.lblSoftlandSpeed.Location = new System.Drawing.Point(359, 66);
            this.lblSoftlandSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSoftlandSpeed.Name = "lblSoftlandSpeed";
            this.lblSoftlandSpeed.Size = new System.Drawing.Size(105, 17);
            this.lblSoftlandSpeed.TabIndex = 512;
            this.lblSoftlandSpeed.Text = "Softland Speed";
            // 
            // txtSoftlandSensitivity
            // 
            this.txtSoftlandSensitivity.Location = new System.Drawing.Point(527, 95);
            this.txtSoftlandSensitivity.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoftlandSensitivity.Name = "txtSoftlandSensitivity";
            this.txtSoftlandSensitivity.Size = new System.Drawing.Size(72, 22);
            this.txtSoftlandSensitivity.TabIndex = 518;
            this.txtSoftlandSensitivity.Text = "100";
            this.txtSoftlandSensitivity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSoftlandSensitivity_KeyUp);
            this.txtSoftlandSensitivity.Leave += new System.EventHandler(this.txtSoftlandSensitivity_Leave);
            // 
            // txtSoftlandSpeed
            // 
            this.txtSoftlandSpeed.Location = new System.Drawing.Point(527, 61);
            this.txtSoftlandSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoftlandSpeed.Name = "txtSoftlandSpeed";
            this.txtSoftlandSpeed.Size = new System.Drawing.Size(72, 22);
            this.txtSoftlandSpeed.TabIndex = 517;
            this.txtSoftlandSpeed.Text = "50";
            this.txtSoftlandSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSpeed_KeyUp);
            this.txtSoftlandSpeed.Leave += new System.EventHandler(this.txtSpeed_Leave);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(605, 65);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(20, 17);
            this.label40.TabIndex = 2;
            this.label40.Text = "%";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(607, 100);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 17);
            this.label20.TabIndex = 2;
            this.label20.Text = "counts";
            // 
            // txtSoftLandForce
            // 
            this.txtSoftLandForce.Location = new System.Drawing.Point(527, 130);
            this.txtSoftLandForce.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoftLandForce.Name = "txtSoftLandForce";
            this.txtSoftLandForce.Size = new System.Drawing.Size(72, 22);
            this.txtSoftLandForce.TabIndex = 573;
            this.txtSoftLandForce.Text = "100";
            this.txtSoftLandForce.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSoftLandForce_KeyUp);
            this.txtSoftLandForce.Leave += new System.EventHandler(this.txtSoftLandForce_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(608, 130);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(20, 17);
            this.label25.TabIndex = 2;
            this.label25.Text = "%";
            // 
            // lblSoftLandForce
            // 
            this.lblSoftLandForce.AutoSize = true;
            this.lblSoftLandForce.Location = new System.Drawing.Point(359, 134);
            this.lblSoftLandForce.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSoftLandForce.Name = "lblSoftLandForce";
            this.lblSoftLandForce.Size = new System.Drawing.Size(100, 17);
            this.lblSoftLandForce.TabIndex = 2;
            this.lblSoftLandForce.Text = "Softland Force";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(609, 134);
            this.label100.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(20, 17);
            this.label100.TabIndex = 572;
            this.label100.Text = "%";
            // 
            // lblLinearDistance_Setup
            // 
            this.lblLinearDistance_Setup.AutoSize = true;
            this.lblLinearDistance_Setup.BackColor = System.Drawing.Color.Transparent;
            this.lblLinearDistance_Setup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.lblLinearDistance_Setup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblLinearDistance_Setup.Location = new System.Drawing.Point(12, 352);
            this.lblLinearDistance_Setup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLinearDistance_Setup.Name = "lblLinearDistance_Setup";
            this.lblLinearDistance_Setup.Size = new System.Drawing.Size(157, 20);
            this.lblLinearDistance_Setup.TabIndex = 570;
            this.lblLinearDistance_Setup.Text = "lblLinearDistance";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1253, 437);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(7, 6);
            this.button2.TabIndex = 518;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.SystemColors.Control;
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(1135, 709);
            this.btnRun.Margin = new System.Windows.Forms.Padding(4);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(160, 43);
            this.btnRun.TabIndex = 120;
            this.btnRun.Text = "INITIALIZE";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnSaveProgramToFile
            // 
            this.btnSaveProgramToFile.Location = new System.Drawing.Point(1132, 223);
            this.btnSaveProgramToFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveProgramToFile.Name = "btnSaveProgramToFile";
            this.btnSaveProgramToFile.Size = new System.Drawing.Size(157, 43);
            this.btnSaveProgramToFile.TabIndex = 120;
            this.btnSaveProgramToFile.Text = "Save Program To File";
            this.btnSaveProgramToFile.UseVisualStyleBackColor = true;
            this.btnSaveProgramToFile.Click += new System.EventHandler(this.btnSaveProgramToFile_Click);
            // 
            // btnSingleCycle
            // 
            this.btnSingleCycle.Location = new System.Drawing.Point(1133, 329);
            this.btnSingleCycle.Margin = new System.Windows.Forms.Padding(4);
            this.btnSingleCycle.Name = "btnSingleCycle";
            this.btnSingleCycle.Size = new System.Drawing.Size(157, 43);
            this.btnSingleCycle.TabIndex = 120;
            this.btnSingleCycle.Text = "Single Cycle";
            this.btnSingleCycle.UseVisualStyleBackColor = true;
            this.btnSingleCycle.Click += new System.EventHandler(this.btnSingleCycle_Click);
            // 
            // btnSaveToController
            // 
            this.btnSaveToController.Location = new System.Drawing.Point(1132, 275);
            this.btnSaveToController.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveToController.Name = "btnSaveToController";
            this.btnSaveToController.Size = new System.Drawing.Size(157, 43);
            this.btnSaveToController.TabIndex = 120;
            this.btnSaveToController.Text = "Save Program To Controller";
            this.btnSaveToController.UseVisualStyleBackColor = true;
            this.btnSaveToController.Click += new System.EventHandler(this.btnSaveInController_Click);
            // 
            // btnSaveSettingsToFile
            // 
            this.btnSaveSettingsToFile.Location = new System.Drawing.Point(1132, 172);
            this.btnSaveSettingsToFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveSettingsToFile.Name = "btnSaveSettingsToFile";
            this.btnSaveSettingsToFile.Size = new System.Drawing.Size(157, 43);
            this.btnSaveSettingsToFile.TabIndex = 110;
            this.btnSaveSettingsToFile.Text = "Save Settings To File";
            this.btnSaveSettingsToFile.UseVisualStyleBackColor = true;
            this.btnSaveSettingsToFile.Click += new System.EventHandler(this.btnSaveSettingsToFile_Click);
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(1135, 761);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(160, 74);
            this.btnStop.TabIndex = 116;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnContactInfo
            // 
            this.btnContactInfo.Location = new System.Drawing.Point(1135, 567);
            this.btnContactInfo.Margin = new System.Windows.Forms.Padding(4);
            this.btnContactInfo.Name = "btnContactInfo";
            this.btnContactInfo.Size = new System.Drawing.Size(160, 33);
            this.btnContactInfo.TabIndex = 117;
            this.btnContactInfo.Text = "Contact Information";
            this.btnContactInfo.UseVisualStyleBackColor = true;
            this.btnContactInfo.Click += new System.EventHandler(this.btnContactInfo_Click);
            // 
            // btnHelpDocument
            // 
            this.btnHelpDocument.Location = new System.Drawing.Point(1135, 619);
            this.btnHelpDocument.Margin = new System.Windows.Forms.Padding(4);
            this.btnHelpDocument.Name = "btnHelpDocument";
            this.btnHelpDocument.Size = new System.Drawing.Size(160, 33);
            this.btnHelpDocument.TabIndex = 117;
            this.btnHelpDocument.Text = "Help Document";
            this.btnHelpDocument.UseVisualStyleBackColor = true;
            this.btnHelpDocument.Click += new System.EventHandler(this.button4_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(965, 514);
            this.listBox1.Margin = new System.Windows.Forms.Padding(4);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(68, 388);
            this.listBox1.TabIndex = 119;
            // 
            // chkShowAll
            // 
            this.chkShowAll.AutoSize = true;
            this.chkShowAll.Location = new System.Drawing.Point(1011, 490);
            this.chkShowAll.Margin = new System.Windows.Forms.Padding(4);
            this.chkShowAll.Name = "chkShowAll";
            this.chkShowAll.Size = new System.Drawing.Size(82, 21);
            this.chkShowAll.TabIndex = 120;
            this.chkShowAll.Text = "Show all";
            this.chkShowAll.UseVisualStyleBackColor = true;
            this.chkShowAll.CheckedChanged += new System.EventHandler(this.chkShowAll_CheckedChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(630, 607);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox2.Size = new System.Drawing.Size(203, 69);
            this.textBox2.TabIndex = 121;
            // 
            // lblStatusTxt
            // 
            this.lblStatusTxt.AutoSize = true;
            this.lblStatusTxt.Location = new System.Drawing.Point(1133, 94);
            this.lblStatusTxt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatusTxt.Name = "lblStatusTxt";
            this.lblStatusTxt.Size = new System.Drawing.Size(48, 17);
            this.lblStatusTxt.TabIndex = 519;
            this.lblStatusTxt.Text = "Status";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(1185, 92);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(104, 18);
            this.lblStatus.TabIndex = 519;
            this.lblStatus.Text = "Status";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(7, 575);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(11, 199);
            this.textBox1.TabIndex = 520;
            this.textBox1.Text = "Di sp l acemen t";
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(257, 832);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 15);
            this.textBox3.TabIndex = 521;
            this.textBox3.Text = "Cycle counts";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SMAC_Leak_Test_Center.Properties.Resources.Leak_Test;
            this.pictureBox1.Location = new System.Drawing.Point(1204, 379);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 101);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 522;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1121, 487);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(181, 73);
            this.pictureBox2.TabIndex = 118;
            this.pictureBox2.TabStop = false;
            // 
            // btnSaveChart
            // 
            this.btnSaveChart.Location = new System.Drawing.Point(384, 487);
            this.btnSaveChart.Name = "btnSaveChart";
            this.btnSaveChart.Size = new System.Drawing.Size(103, 23);
            this.btnSaveChart.TabIndex = 524;
            this.btnSaveChart.TabStop = false;
            this.btnSaveChart.Text = "Save Chart";
            this.btnSaveChart.UseVisualStyleBackColor = true;
            this.btnSaveChart.Click += new System.EventHandler(this.btnSaveChart_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1128, 437);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 20);
            this.label17.TabIndex = 525;
            this.label17.Text = "V0.17";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1312, 864);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.btnSaveChart);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblStatusTxt);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.chkShowAll);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnHelpDocument);
            this.Controls.Add(this.btnContactInfo);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnSaveProgramToFile);
            this.Controls.Add(this.txtTerminal);
            this.Controls.Add(this.btnSingleCycle);
            this.Controls.Add(this.btnSaveToController);
            this.Controls.Add(this.btnLoadSettingsFromFile);
            this.Controls.Add(this.btnSaveSettingsToFile);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.cmbPort);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMAC Leak Test Center";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.Run.ResumeLayout(false);
            this.Run.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRun)).EndInit();
            this.Setup.ResumeLayout(false);
            this.Setup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSetup)).EndInit();
            this.Tuning.ResumeLayout(false);
            this.Tuning.PerformLayout();
            this.gbxTeachResult1.ResumeLayout(false);
            this.gbxTeachResult1.PerformLayout();
            this.gbxTuningLinear.ResumeLayout(false);
            this.gbxTuningLinear.PerformLayout();
            this.gbxTeachResult.ResumeLayout(false);
            this.gbxTeachResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPositionCloseToPart;
        public System.Windows.Forms.TextBox txtPositionCloseToContainer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblThreadDepth;
        public System.Windows.Forms.TextBox txtThreadDepth;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblActuator;
        private System.Windows.Forms.Label lblPort;
        public System.Windows.Forms.TextBox txtTerminal;
        private System.Windows.Forms.Button btnLoadSettingsFromFile;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnActuatorInfo;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Run;
        private System.Windows.Forms.TabPage Setup;
        private System.Windows.Forms.PictureBox pictureBoxSetup;
        public System.Windows.Forms.TextBox txtPitch;
        private System.Windows.Forms.Label lblLandedDistanceMin;
        public System.Windows.Forms.TextBox txtLandedDistanceMax;
        public System.Windows.Forms.TextBox txtLandedDistanceMin;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblSoftLandForce;
        private System.Windows.Forms.Button btnSaveToController;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnContactInfo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnHelpDocument;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnSaveProgramToFile;
        private System.Windows.Forms.Button btnSingleCycle;
        private System.Windows.Forms.Button btnTeach;
        private System.Windows.Forms.Button btnSaveSettingsToFile;
        private System.Windows.Forms.Label lblSoftlandSpeed;
        public System.Windows.Forms.TextBox txtTuningLinearD;
        public System.Windows.Forms.TextBox txtTuningLinearI;
        public System.Windows.Forms.TextBox txtTuningLinearP;
        public System.Windows.Forms.TextBox txtSoftlandSpeed;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lblTuningLinearDUnits;
        private System.Windows.Forms.Label lblTuningLinearIUnits;
        private System.Windows.Forms.Label lblTuningLinearPUnits;
        private System.Windows.Forms.Label lblTuningLinearD;
        private System.Windows.Forms.Label lblTuningLinearI;
        private System.Windows.Forms.Label lblTuningLinearP;
        public System.Windows.Forms.ComboBox cmbActuator;
        public System.Windows.Forms.CheckBox chkCheckLandedDistance;
        public System.Windows.Forms.ComboBox cmbPort;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkShowAll;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.PictureBox pictureBoxRun;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Label lblTeachedSqLinear;
        public System.Windows.Forms.ComboBox cmbPitchUnits;
        public System.Windows.Forms.TextBox txtTeachedSqLinear;
        public System.Windows.Forms.TextBox txtTeachedLandedDis;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lblTeachedLandedDis;
        private System.Windows.Forms.Label lblTeachedHeightUnits;
        private System.Windows.Forms.Label lblSoftlandSensitivity;
        public System.Windows.Forms.TextBox txtSoftlandSensitivity;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox gbxTeachResult;
        public System.Windows.Forms.TextBox txtTeachedSqLinearHome;
        private System.Windows.Forms.Label lblTeachedSqLinearHome;
        private System.Windows.Forms.Label lblLandedDistanceMax;
        private System.Windows.Forms.TabPage Tuning;
        private System.Windows.Forms.GroupBox gbxTuningRotary;
        private System.Windows.Forms.GroupBox gbxTuningLinear;
        private System.Windows.Forms.Label lblStatusTxt;
        public System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblSoftlandPositionSetup;
        private System.Windows.Forms.Label lblDisplacementRun;
        public System.Windows.Forms.ComboBox cmbLeakTestType;
        private System.Windows.Forms.Label lblLeakTestType;
        public System.Windows.Forms.TextBox txtParkPosition;
        private System.Windows.Forms.Label lblParkPosition;
        private System.Windows.Forms.TextBox txtSoftlandPosition;
        public System.Windows.Forms.TextBox txtMaxDisplacement;
        private System.Windows.Forms.Label lblMaxDispLim;
        private System.Windows.Forms.Label lblLinearDistance_Setup;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label lblController;
        public System.Windows.Forms.ComboBox cmbController;
        private System.Windows.Forms.Label lblFinalPosition;
        private System.Windows.Forms.Label lblDisplacement;
        private System.Windows.Forms.TextBox txtFinalPosition;
        private System.Windows.Forms.TextBox txtDisplacement;
        public System.Windows.Forms.TextBox txtPushForce;
        private System.Windows.Forms.Label lblPushForce;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label100;
        public System.Windows.Forms.TextBox txtSoftLandForce;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtDwellTime;
        private System.Windows.Forms.Label lblDwellTime;

        private System.Windows.Forms.Label lblSoftlandPosition;
        private System.Windows.Forms.Label lblDisplacementRun1;
        private System.Windows.Forms.Label lblDisplacementSetup1;
        private System.Windows.Forms.Label lblDisplacementSetup;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox gbxTeachResult1;
        private System.Windows.Forms.Button btnUndo1;
        public System.Windows.Forms.TextBox txtTeached2LandedDis;
        public System.Windows.Forms.TextBox txtTeached2SqLinear;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtSoftlandSensitivity2;
        public System.Windows.Forms.TextBox txtSpeed;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnSaveChart;
        private System.Windows.Forms.Label label17;
    }
}

