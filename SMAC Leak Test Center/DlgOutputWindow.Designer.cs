﻿namespace SMAC_Leak_Test_Center
{
    partial class dlgOutputWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.chkAddtimestamp = new System.Windows.Forms.CheckBox();
            this.chkAddname = new System.Windows.Forms.CheckBox();
            this.txtLogwindow = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(199, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(199, 54);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // chkAddtimestamp
            // 
            this.chkAddtimestamp.AutoSize = true;
            this.chkAddtimestamp.Location = new System.Drawing.Point(13, 13);
            this.chkAddtimestamp.Name = "chkAddtimestamp";
            this.chkAddtimestamp.Size = new System.Drawing.Size(123, 21);
            this.chkAddtimestamp.TabIndex = 2;
            this.chkAddtimestamp.Text = "Add timestamp";
            this.chkAddtimestamp.UseVisualStyleBackColor = true;
            // 
            // chkAddname
            // 
            this.chkAddname.AutoSize = true;
            this.chkAddname.Location = new System.Drawing.Point(13, 41);
            this.chkAddname.Name = "chkAddname";
            this.chkAddname.Size = new System.Drawing.Size(132, 21);
            this.chkAddname.TabIndex = 3;
            this.chkAddname.Text = "Add objectname";
            this.chkAddname.UseVisualStyleBackColor = true;
            // 
            // txtLogwindow
            // 
            this.txtLogwindow.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLogwindow.Location = new System.Drawing.Point(13, 97);
            this.txtLogwindow.Multiline = true;
            this.txtLogwindow.Name = "txtLogwindow";
            this.txtLogwindow.ReadOnly = true;
            this.txtLogwindow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLogwindow.Size = new System.Drawing.Size(1048, 530);
            this.txtLogwindow.TabIndex = 4;
            // 
            // dlgOutputWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.txtLogwindow);
            this.Controls.Add(this.chkAddname);
            this.Controls.Add(this.chkAddtimestamp);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSave);
            this.Name = "dlgOutputWindow";
            this.Text = "DlgOutputWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.CheckBox chkAddtimestamp;
        private System.Windows.Forms.CheckBox chkAddname;
        private System.Windows.Forms.TextBox txtLogwindow;
    }
}