﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAC_Leak_Test_Center
{
    //
    //
    // The ValueChecker class is used to check if one of the variables has been changed
    //
    // First call Load() - This will read the value of all variables on the mainform
    // Call Changed() to check if one of the values has been changed since the last call to Load.
    //
    class ValueChecker
    {
        private string text1 = "";
        private string text2 = "";
        private string text3 = "";
        private string text4 = "";
        private string text5 = "";
        private string text6 = "";
        private string text7 = "";

        private bool bool1 = false;
        private bool bool2 = false;
        private bool bool3 = false;
        private bool bool4 = false;
        private bool bool5 = false;


        private double double1 = 0;
        private double double2 = 0;
        private double double3 = 0;
        private double double4 = 0;
        private double double5 = 0;
        private double double6 = 0;
        
        private double double8 = 0;
        private double double9 = 0;
        private double double10 = 0;


        private double double13 = 0;
        private double double14 = 0;
      //  private double double15 = 0;
        
        private double double16 = 0;
      //  private double double17 = 0;
        private double double18 = 0;
        private double double19 = 0;
        private double double20 = 0;




       

        public void Load()
        {
            text1 = Globals.mainForm.cmbActuator.SelectedItem.ToString();
            text4 = Globals.mainForm.cmbLeakTestType.ToString();  
            text5 = Globals.mainForm.txtTeachedSqLinear.Text;
            text6 = Globals.mainForm.txtTeachedLandedDis.Text;
            text7 = Globals.mainForm.txtTeachedSqLinearHome.Text;

            bool1 = Globals.mainForm.chkCheckLandedDistance.Checked;
            double1 = Globals.mainForm.ibdPositonCloseToContainer.Doublevalue;
            double2 = Globals.mainForm.ibdLandedHeightMin.Doublevalue;
            double3 = Globals.mainForm.ibdLandedHeightMax.Doublevalue;
            double4 = Globals.mainForm.ibdPushForce.Doublevalue;
            double5 = Globals.mainForm.ibdDwellTime.Doublevalue;     
            double6 = Globals.mainForm.ibdParkPosition.Doublevalue;
             

            double8 = Globals.mainForm.ibdTuningLinearP.Doublevalue;
            double9 = Globals.mainForm.ibdTuningLinearI.Doublevalue;
            double10 = Globals.mainForm.ibdTuningLinearD.Doublevalue;

            
            
            

            
            double13 = Globals.mainForm.ibdSpeed.Doublevalue;
            double14 = Globals.mainForm.ibdSoftlandSpeed.Doublevalue;
          //  double15 = Globals.mainForm.ibdSoftlandForceOffsetTeach.Doublevalue;

            double16 = Globals.mainForm.ibdSoftlandPosition.Doublevalue;
           // double17 = Globals.mainForm.ibdSetResolution.Doublevalue;
            double18 = Globals.mainForm.ibdMaxDispLim.Doublevalue;    
               

            double19 = Globals.mainForm.ibdSoftlandSensitivity.Doublevalue;
            double20 = Globals.mainForm.ibdSoftlandSensitivity2.Doublevalue;

        }

        public bool Changed()
        {
            return
                (text1 != Globals.mainForm.cmbActuator.SelectedItem.ToString()) ||
               
                (text4 != Globals.mainForm.cmbLeakTestType.Text) ||
                (text5 != Globals.mainForm.txtTeachedSqLinear.Text) ||
                (text6 != Globals.mainForm.txtTeachedLandedDis.Text) ||
                (text7 != Globals.mainForm.txtTeachedSqLinearHome.Text) ||

                (bool1 != Globals.mainForm.chkCheckLandedDistance.Checked) ||

                (double1 != Globals.mainForm.ibdPositonCloseToContainer.Doublevalue) ||
                (double2 != Globals.mainForm.ibdLandedHeightMin.Doublevalue) ||
                (double3 != Globals.mainForm.ibdLandedHeightMax.Doublevalue) ||
                (double4 != Globals.mainForm.ibdPushForce.Doublevalue) ||
                (double5 != Globals.mainForm.ibdDwellTime.Doublevalue) ||
                (double6 != Globals.mainForm.ibdParkPosition.Doublevalue) ||
                (double8 != Globals.mainForm.ibdTuningLinearP.Doublevalue) ||
                (double9 != Globals.mainForm.ibdTuningLinearI.Doublevalue) ||
                (double10 != Globals.mainForm.ibdTuningLinearD.Doublevalue) ||
                (double13 != Globals.mainForm.ibdSpeed.Doublevalue) ||
                (double14 != Globals.mainForm.ibdSoftlandSpeed.Doublevalue) ||
             //   (double15 != Globals.mainForm.ibdSoftlandForceOffsetTeach.Doublevalue) ||
                (double16 != Globals.mainForm.ibdSoftlandPosition.Doublevalue) ||
                //  (double17 != Globals.mainForm.ibdSetResolution.Doublevalue) ||
                (double18 != Globals.mainForm.ibdMaxDispLim.Doublevalue) ||
                (double19 != Globals.mainForm.ibdSoftlandSensitivity.Doublevalue) ||
                (double20 != Globals.mainForm.ibdSoftlandSensitivity2.Doublevalue);
    






        }
    }
}
