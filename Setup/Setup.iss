; Based on -- ISPPExample1.iss --
;
; This script shows various basic things you can achieve using Inno Setup Preprocessor (ISPP).
; To enable commented #define's, either remove the ';' or use ISCC with the /D switch.


[Setup]
AppName=SMAC Leak Test Center
AppVersion=0.17
DefaultDirName={pf}\SMAC\SMAC Leak Test Center
DefaultGroupName=SMAC Leak Test Center
UninstallDisplayIcon={app}\SMAC Leak Test Center.exe
;LicenseFile=Files\License.txt
OutputDir=.\Output

[InstallDelete]
Type: files; Name: "{app}\Actuators\*"

[Files]
Source: "Files\SMAC Leak Test Center.exe"; DestDir: "{app}"; Flags: ignoreversion 
Source: "..\Documenten\Help document.pdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "Files\Actuators\*"; DestDir: "{app}\Actuators"; Flags: ignoreversion recursesubdirs

; Source: "Readme.txt"; DestDir: "{app}"; \
;  Flags: isreadme

[Icons]
Name: "{commonprograms}\SMAC Leak Test Center"; Filename: "{app}\SMAC Leak Test Center.exe"
Name: "{commondesktop}\SMAC Leak Test Center"; Filename: "{app}\SMAC Leak Test Center.exe"

[Run]
Filename: "{app}\SMAC Leak Test Center.exe"; Description: "Launch SMAC Leak Test Center"; Flags: postinstall nowait skipifsilent

